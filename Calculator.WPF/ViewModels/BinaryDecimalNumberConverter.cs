﻿namespace Calculator.WPF.ViewModels
{
   using System;
   using System.Globalization;
   using System.Windows.Data;
   using ValueConverter;

   /// <summary>
   /// Converts a string representing a value as a binary number into its decimal representation.
   /// Just a wrapper for a INumberConverter.
   /// </summary>
   /// <seealso cref="System.Windows.Data.IValueConverter" />
   public class BinaryDecimalNumberConverter : IValueConverter
   {
      /// <summary>
      /// The converter used for the conversion.
      /// </summary>
      private INumberConverter converter = new BinaryDecimalConverter();

      /// <summary>
      /// Converts the specified value to a string representing the value as a decimal number.
      /// </summary>
      /// <param name="value">The value. Must equal a binary number.</param>
      /// <param name="targetType">Type of the target.</param>
      /// <param name="parameter">The parameter.</param>
      /// <param name="culture">The culture.</param>
      /// <returns>A string representing the value as a decimal number.</returns>
      /// <exception cref="InvalidOperationException">
      /// Thrown if this is used before Division was set.
      /// </exception>
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return this.converter.ConvertBinaryToDecimal(value as string);
      }

      /// <summary>
      /// NOT YET IMPLEMENTED!!!
      /// </summary>
      /// <param name="value">The value.</param>
      /// <param name="targetType">Type of the target.</param>
      /// <param name="parameter">The parameter.</param>
      /// <param name="culture">The culture.</param>
      /// <returns>A System.Object.</returns>
      /// <exception cref="NotImplementedException">
      /// Always thrown, because this function is not yet implemented, but part of the interface.
      /// </exception>
      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}