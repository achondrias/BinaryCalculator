﻿namespace Calculator.WPF.ViewModels
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Linq;
   using System.Runtime.CompilerServices;
   using Base;

   /// <summary>
   /// A ViewModel for a binary calculation. Consists of two operands, an operator and a result.
   /// </summary>
   /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
   public class BinaryCalculation : ICalculation
   {
      #region Fields 
      /// <summary>
      /// The operations available on the ICalculator member.
      /// </summary>
      private IEnumerable<char> availableOperations;

      /// <summary>
      /// The calculator used as the model.
      /// </summary>
      private ICalculator calculator;

      /// <summary>
      /// Indicates whether the current state can be used for a calculation.
      /// E.g. if the result would be negative, it cannot be calculated.
      /// </summary>
      private bool canCalculate;

      /// <summary>
      /// The error explaining why the last calculation failed.
      /// </summary>
      private string error;

      /// <summary>
      /// The left operand.
      /// </summary>
      private string leftOperand = "0";

      /// <summary>
      /// The right operand.
      /// </summary>
      private string rightOperand = "0";

      /// <summary>
      /// The result.
      /// </summary>
      private string result = "0";

      /// <summary>
      /// The operator sign that signifies the type of calculation.
      /// </summary>
      private char operatorSign;
      #endregion

      #region Events      
      /// <summary>
      /// Occurs when one of the properties changed.
      /// </summary>
      public event PropertyChangedEventHandler PropertyChanged;
      #endregion

      #region Properties      
      /// <summary>
      /// Gets the available operations.
      /// </summary>
      /// <value>
      /// The available operations.
      /// </value>
      public IEnumerable<char> AvailableOperations
      {
         get
         {
            if (this.availableOperations == null)
            {
               this.availableOperations = this.calculator.AvailableOperations;
            }

            return this.availableOperations;
         }
      }

      /// <summary>
      /// Gets a value indicating whether this instance is a valid calculation.
      /// Both operands must be valid and the operator must be applicable on them.
      /// </summary>
      /// <value>
      ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
      /// </value>
      public bool IsValid
      {
         get
         {
            return this.canCalculate;
         }

         private set
         {
            this.SetProperty(ref this.canCalculate, value);
         }
      }

      /// <summary>
      /// Gets or sets the error explaining why the last calculation failed.
      /// </summary>
      /// <value>
      /// The error message.
      /// </value>
      public string Error
      {
         get
         {
            return this.error;
         }

         set
         {
            this.SetProperty(ref this.error, value);
         }
      }

      /// <summary>
      /// Gets or sets the Calculator to use.
      /// When <paramref name="value"/> is null, it will not be set.
      /// </summary>
      public ICalculator Calculator
      {
         get
         {
            return this.calculator;
         }

         set
         {
            if (value != null)
            {
               this.SetProperty(ref this.calculator, value);
            }
         }
      }

      /// <summary>
      /// Gets or sets the left operand.
      /// Setting triggers a calculation.
      /// </summary>
      /// <value>The left operand. Must consist of only '0' and '1'.</value>
      public string LeftOperand
      {
         get
         {
            return this.leftOperand;
         }

         set
         {
            this.SetProperty(ref this.leftOperand, value);
            this.Result = this.Calculate();
         }
      }

      /// <summary>
      /// Gets or sets the right operand.
      /// Setting triggers a calculation.
      /// </summary>
      /// <value>The right operand. Must consist of only '0' and '1'.</value>
      public string RightOperand
      {
         get
         {
            return this.rightOperand;
         }

         set
         {
            this.SetProperty(ref this.rightOperand, value);
            this.Result = this.Calculate();
         }
      }

      /// <summary>
      /// Gets or sets the operator sign.
      /// Setting triggers a calculation.
      /// </summary>
      /// <value>
      /// The operator sign. The underlying calculator must have a corresponding operation.
      /// </value>
      public char OperatorSign
      {
         get
         {
            if (this.operatorSign == 0)
            {
               this.operatorSign = this.AvailableOperations.First();
            }

            return this.operatorSign;
         }

         set
         {
            this.SetProperty(ref this.operatorSign, value);
            this.Result = this.Calculate();
         }
      }

      /// <summary>
      /// Gets the result.
      /// Recalculated when one the operands or the operator is set.
      /// </summary>
      /// <value>The result.</value>
      public string Result
      {
         get
         {
            return this.result;
         }

         private set
         {
            this.SetProperty(ref this.result, value);
         }
      }
      #endregion

      #region Methods      
      /// <summary>
      /// Sets the calling property and its corresponding field.
      /// Fires of the INotifyPropertyChanged event.
      /// </summary>
      /// <typeparam name="T">Type of the property. May be omitted.</typeparam>
      /// <param name="field">The field backing the calling property.</param>
      /// <param name="newValue">The new value.</param>
      /// <param name="propertyName">Name of the property. May be omitted.</param>
      /// <returns>
      /// <c>true</c> if a new value was set, <c>false</c> otherwise.
      /// If the current and the new value are equal, no new value will be set.
      /// </returns>
      protected bool SetProperty<T>(
         ref T field,
         T newValue,
         [CallerMemberName] string propertyName = null)
      {
         if (!EqualityComparer<T>.Default.Equals(field, newValue))
         {
            field = newValue;
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));

            return true;
         }

         return false;
      }

      /// <summary>
      /// Handles the <see cref="E:PropertyChanged" /> event.
      /// </summary>
      /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
      private void OnPropertyChanged(PropertyChangedEventArgs e)
      {
         this.PropertyChanged?.Invoke(this, e);
      }

      /// <summary>
      /// Calculates the result of the currently set calculation, 
      /// i.e. the left operand, the operator, and the right operand.
      /// </summary>
      /// <returns>The result of the calculation.</returns>
      /// <exception cref="InvalidOperationException">
      /// Thrown if no result could be calculated. 
      /// For detailed explanation why it failed, the inner exception should be used.
      /// </exception>
      private string Calculate()
      {
         if (this.calculator == null)
         {
            throw new InvalidOperationException(
               nameof(this.Calculator) +
               " not yet set. Finish initialization of this object before using it.");
         }

         var leftOperand = this.LeftOperand;
         var rightOperand = this.RightOperand;
         string calculationResult;
         string errorMessage;

         if (this.calculator.CanCalculate(
            this.OperatorSign, 
            leftOperand, 
            rightOperand, 
            out errorMessage))
         {
            calculationResult = this.calculator
               .Calculate(this.OperatorSign, leftOperand, rightOperand)
               .ToString();
            this.IsValid = true;
            return calculationResult;
         }
         else
         {
            this.Error = errorMessage;
            this.IsValid = false;
            return this.Result;
         }
      }
      #endregion
   }
}