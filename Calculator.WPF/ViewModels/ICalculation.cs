﻿namespace Calculator.WPF.ViewModels
{
   using System.Collections.Generic;
   using System.ComponentModel;

   /// <summary>
   /// Defines a binary calculation.
   /// </summary>
   /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
   public interface ICalculation : INotifyPropertyChanged
   {
      /// <summary>
      /// Gets the available operations.
      /// </summary>
      /// <value>
      /// The available operations.
      /// </value>
      IEnumerable<char> AvailableOperations { get; }

      /// <summary>
      /// Gets a value indicating whether this instance is a valid calculation.
      /// Both operands must be valid and the operator must be applicable on them.
      /// </summary>
      /// <value>
      ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
      /// </value>
      bool IsValid { get; }

      /// <summary>
      /// Gets the error explaining why the last calculation failed..
      /// </summary>
      /// <value>
      /// The error message.
      /// </value>
      string Error { get; }

      /// <summary>
      /// Gets or sets the left operand.
      /// </summary>
      /// <value>
      /// The left operand.
      /// </value>
      string LeftOperand { get; set; }

      /// <summary>
      /// Gets or sets the right operand.
      /// </summary>
      /// <value>
      /// The right operand.
      /// </value>
      string RightOperand { get; set; }

      /// <summary>
      /// Gets or sets the operator sign.
      /// </summary>
      /// <value>
      /// The operator sign.
      /// </value>
      char OperatorSign { get; set; }

      /// <summary>
      /// Gets the result.
      /// </summary>
      /// <value>
      /// The result.
      /// </value>
      string Result { get; }
   }
}