﻿namespace Calculator.WPF
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Windows;
   using Calculator.Base;
   using Calculator.Base.Operations;
   using ValueConverter;

   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      /// <summary>
      /// Raises the <see cref="E:System.Windows.Application.Startup" /> event.
      /// Bootstraps the application.
      /// </summary>
      /// <param name="e">A <see cref="T:System.Windows.StartupEventArgs" /> that contains the event data.</param>
      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);

         List<Operation> operators;
         Calculator calculator;
         ViewModels.BinaryCalculation viewModel;
         MainWindow window;

         operators = new List<Operation>()
         {
            new Addition(),
            new Multiplication(),
            new Subtraction(),
            new Division()
         };
         calculator = new Calculator(operators);
         viewModel = new ViewModels.BinaryCalculation();
         viewModel.Calculator = calculator;
         window = new MainWindow();
         viewModel.OperatorSign = calculator.AvailableOperations.First();
         window.DataContext = viewModel as ViewModels.ICalculation;

         window.Show();
      }
   }
}