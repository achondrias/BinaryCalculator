﻿namespace Calculator.WPF.Views.Decimal
{
   using System.Windows.Controls;

   /// <summary>
   /// Interaction logic for DecimalCalculation.xaml
   /// </summary>
   public partial class DecimalCalculation : UserControl
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalCalculation"/> class.
      /// </summary>
      public DecimalCalculation()
      {
         this.InitializeComponent();
      }
   }
}
