﻿namespace Calculator.WPF.Views.Decimal
{
   using System.Windows.Controls;

   /// <summary>
   /// Interaction logic for DecimalNumberArea.xaml
   /// </summary>
   public partial class DecimalNumberArea : UserControl
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalNumberArea"/> class.
      /// </summary>
      public DecimalNumberArea()
      {
         this.InitializeComponent();
      }
   }
}
