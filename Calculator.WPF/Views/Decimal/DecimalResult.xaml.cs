﻿namespace Calculator.WPF.Views.Decimal
{
   using System.Windows.Controls;

   /// <summary>
   /// Interaction logic for DecimalResult.xaml
   /// </summary>
   public partial class DecimalResult : UserControl
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalResult"/> class.
      /// </summary>
      public DecimalResult()
      {
         this.InitializeComponent();
      }
   }
}
