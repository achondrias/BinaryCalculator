﻿namespace Calculator.WPF.Views.Binary
{
   using System.Windows.Controls;

   /// <summary>
   /// Interaction logic for BinaryResult.xaml
   /// </summary>
   public partial class BinaryResult : UserControl
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="BinaryResult"/> class.
      /// </summary>
      public BinaryResult()
      {
         this.InitializeComponent();
      }
   }
}
