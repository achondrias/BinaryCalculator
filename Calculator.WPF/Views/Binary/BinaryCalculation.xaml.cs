﻿namespace Calculator.WPF.Views.Binary
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Windows.Controls;
   using System.Windows.Input;
   using ViewModels;

   /// <summary>
   /// Interaction logic for BinaryCalculation.xaml
   /// </summary>
   public partial class BinaryCalculation : UserControl
   {
      /// <summary>
      /// Since this view is for a binary calculator, only '1' and '0' are valid input characters.
      /// </summary>
      private string allowedCharacters = "01";

      /// <summary>
      /// The number zero.
      /// </summary>
      private string zero = "0";

      /// <summary>
      /// Initializes a new instance of the <see cref="BinaryCalculation"/> class.
      /// </summary>
      public BinaryCalculation()
      {
         this.InitializeComponent();
      }

      /// <summary>
      /// Resets this instance.
      /// </summary>
      private void Reset()
      {
         var viewmodel = this.DataContext as ICalculation;
         viewmodel.LeftOperand = this.zero;
         viewmodel.RightOperand = this.zero;
         viewmodel.OperatorSign = viewmodel.AvailableOperations.First();
      }

      /// <summary>
      /// Handles the PreviewTextInput event of the TextBox control.
      /// Checks if the input was valid.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="TextCompositionEventArgs"/> instance containing the event data.</param>
      private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
      {
         var addedSymbol = e.Text.Length > 0 ? e.Text.First() : char.MinValue;
         var viewModel = this.DataContext as ICalculation;

         if (viewModel.AvailableOperations.Contains(addedSymbol))
         {
            viewModel.OperatorSign = addedSymbol;
            e.Handled = true;
         }
         else if (e.Text.Any(c => !this.allowedCharacters.Contains(c)))
         {
            e.Handled = true;
         }
      }

      /// <summary>
      /// Handles the PreviewKeyDown event of the TextBox control.
      /// Used to check for keyboard input, that is not caught by PreviewTextInput.
      /// This includes e.g. space.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
      private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Escape)
         {
            this.Reset();
            e.Handled = true;
         }

         if (e.Key == Key.Space)
         {
            e.Handled = true;
         }
      }
   }
}