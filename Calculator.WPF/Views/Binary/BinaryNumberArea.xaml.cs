﻿namespace Calculator.WPF.Views.Binary
{
   using System.Windows.Controls;

   /// <summary>
   /// Interaction logic for BinaryNumberArea.xaml
   /// </summary>
   public partial class BinaryNumberArea : UserControl
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="BinaryNumberArea"/> class.
      /// </summary>
      public BinaryNumberArea()
      {
         this.InitializeComponent();
      }
   }
}
