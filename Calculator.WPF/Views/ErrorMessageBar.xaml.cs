﻿namespace Calculator.WPF.Views
{
   using System.Windows.Controls;

   /// <summary>
   /// Interaction logic for ErrorMessageBar.xaml
   /// </summary>
   public partial class ErrorMessageBar : UserControl
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="ErrorMessageBar"/> class.
      /// </summary>
      public ErrorMessageBar()
      {
         this.InitializeComponent();
      }
   }
}
