﻿namespace Calculator.Base.Digits
{
   using System;

   /// <summary>
   /// Base class for digits. Defines a numeral system by setting the range of allowed values.
   /// </summary>
   public abstract class Digit : IEquatable<Digit>, IComparable<Digit>
   {
      #region Properties
      /// <summary>
      /// Gets the character representation of this digit.
      /// </summary>
      public abstract char Numeral { get; }

      /// <summary>
      /// Gets the value of the digit.
      /// </summary>
      protected abstract int Value { get; }
      #endregion

      #region Operators
      /// <summary>
      /// Compares the values of both digits.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if both digits have the same value, else false.</returns>
      public static bool operator ==(Digit left, Digit right)
      {
         return left.Equals(right);
      }

      /// <summary>
      /// Compares the values of both digits.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if both digits have a different value, else false.</returns>
      public static bool operator !=(Digit left, Digit right)
      {
         return !(left == right);
      }

      /// <summary>
      /// Compares the values of both digits.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side digit is less than 
      /// the right-hand side digit.</returns>
      public static bool operator <(Digit left, Digit right)
      {
         return left.CompareTo(right) < 0 ? true : false;
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side digit is greater than 
      /// the right-hand side digit.</returns>
      public static bool operator >(Digit left, Digit right)
      {
         return left.CompareTo(right) > 0 ? true : false;
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side digit is not greater than 
      /// the right-hand side digit.</returns>
      public static bool operator <=(Digit left, Digit right)
      {
         return left.CompareTo(right) < 1 ? true : false;
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side digit is not less than 
      /// the right-hand side digit.</returns>
      public static bool operator >=(Digit left, Digit right)
      {
         return left.CompareTo(right) > -1 ? true : false;
      }
      #endregion

      #region Public Methods
      /// <summary>
      /// Compares the value of this and another's digit.
      /// </summary>
      /// <param name="other">The digit to compare with.</param>
      /// <returns>1 if the value of this is greater.
      /// 0 if both numbers have the same value.
      /// -1 if the value of this is less.
      /// Null is always considered lesser.</returns>
      public virtual int CompareTo(Digit other)
      {
         if (this.GetType() != other.GetType())
         {
            throw new ArgumentException(
               "The comparand is a different kind of digit!",
               other.GetType().ToString());
         }

         return object.ReferenceEquals(other, null) ? 1 : this.Value - other.Value;
      }

      /// <summary>
      /// Compares the values of this and the given parameter.
      /// </summary>
      /// <param name="obj">The object to compare with.</param>
      /// <returns>False if object either is not of the same type 
      /// or has a different value. Else true.</returns>
      public override bool Equals(object obj)
      {
         if (object.ReferenceEquals(obj, null))
         {
            return object.ReferenceEquals(this, null);
         }
         else
         {
            return this.GetType() == obj.GetType() ? this.Equals((Digit)obj) : false;
         }
      }

      /// <summary>
      /// Compares the values of this and the given parameter.
      /// </summary>
      /// <param name="other">The digit to compare with.</param>
      /// <returns>True if both have the same value. Else false.</returns>
      public bool Equals(Digit other)
      {
         if (this.GetType() != other.GetType())
         {
            return false;
         }

         return object.ReferenceEquals(other, null) ?
            object.ReferenceEquals(this, null) :
            this.Value == other.Value;
      }

      /// <summary>
      /// Computes a hash code for this digit based on its value.
      /// </summary>
      /// <returns>A hash code for this digit.</returns>
      public override int GetHashCode()
      {
         return this.Value.GetHashCode();
      }

      /// <summary>
      /// Returns a string representation of this digit.
      /// </summary>
      /// <returns>A string representing this digit.</returns>
      public override string ToString()
      {
         return new string(new char[] { this.Numeral });
      }
      #endregion
   }
}