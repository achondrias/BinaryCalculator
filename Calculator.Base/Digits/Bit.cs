﻿namespace Calculator.Base.Digits
{
   using System;
   using System.Collections.Generic;

   /// <summary>
   /// Represents a binary digit. Values range from 0 - 1.
   /// </summary>
   public class Bit : Digit
   {
      #region Fields
      /// <summary>
      /// Represents the value zero. Used for comparison.
      /// </summary>
      public const char Zero = '0';

      /// <summary>
      /// Represents the value one. Used for comparison.
      /// </summary>
      public const char One = '1';

      /// <summary>
      /// The range of allowed values.
      /// </summary>
      private static readonly List<char> AllowedValues = new List<char> { '0', '1' };
      #endregion

      #region Constructors
      /// <summary>
      /// Initializes a new instance of the <see cref="Bit" /> class 
      /// with its value set to zero.
      /// </summary>
      public Bit()
      {
         this.Value = 0;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="Bit" /> class 
      /// with its value set to the value of the parameter.
      /// </summary>
      /// <param name="value">Must be either 0 or 1.</param>
      public Bit(int value)
      {
         if (!Bit.CanConvert(value))
         {
            throw new ArgumentOutOfRangeException("A bit can only be 0 or 1.", nameof(value));
         }

         this.Value = value;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="Bit" /> class 
      /// with its value set to the value of the parameter.
      /// </summary>
      /// <param name="value">Must be either Bit.Zero or Bit.One.</param>
      public Bit(char value)
      {
         if (!Bit.CanConvert(value))
         {
            throw new ArgumentException("Argument does not represent a bit.", nameof(value));
         }

         this.Value = (int)value - 48; // 48 == ASCII '0'
      }
      #endregion

      #region Properties
      /// <summary>
      /// Gets the value of this digit as a character representation.
      /// </summary>
      public override char Numeral
      {
         get
         {
            return (char)(48 + this.Value); // 48 == ASCII for '0'
         }
      }

      /// <summary>
      /// Gets the value of this digit.
      /// </summary>
      protected override int Value { get; }
      #endregion

      #region Operators
      /// <summary>
      /// XOR operator for bits.
      /// </summary>
      /// <param name="left">Left-hand side.</param>
      /// <param name="right">Right-hand side.</param>
      /// <returns>True if exactly one of the operands is one. Else false.</returns>
      public static Bit operator ^(Bit left, Bit right)
      {
         return new Bit(left.Value ^ right.Value);
      }
      #endregion

      #region Public Methods
      /// <summary>
      /// Verifies if the input can be converted to this digit type.
      /// </summary>
      /// <param name="input">A character possibly representing a bit.</param>
      /// <returns>True if input can be used to create a bit, else false.</returns>
      public static bool CanConvert(char input)
      {
         return AllowedValues.Contains(input);
      }

      /// <summary>
      /// Verifies if the input can be converted to this digit type.
      /// </summary>
      /// <param name="input">An integer possibly within the range of a bit.</param>
      /// <returns>True if input can be used to create a bit, else false.</returns>
      public static bool CanConvert(int input)
      {
         return input == 0 || input == 1;
      }
      #endregion
   }
}