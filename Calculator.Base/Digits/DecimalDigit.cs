﻿namespace Calculator.Base.Digits
{
   using System;
   using System.Collections.Generic;

   /// <summary>
   /// Represents a decimal digit. Values range from 0 - 9.
   /// </summary>
   public class DecimalDigit : Digit
   {
      #region Fields
      /// <summary>
      /// Represents the value zero. Used for comparison.
      /// </summary>
      public const char Zero = '0';

      /// <summary>
      /// The range of allowed values.
      /// </summary>
      private static readonly List<char> AllowedValue = new List<char>
      {
         '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
      };

      #endregion

      #region Constructors
      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalDigit" /> class 
      /// with its value set to zero.
      /// </summary>
      public DecimalDigit()
      {
         this.Value = 0;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalDigit" /> class 
      /// with its value set to the value of the parameter.
      /// </summary>
      /// <param name="value">Must lie between 0 and 9.</param>
      public DecimalDigit(int value)
      {
         if (!DecimalDigit.CanConvert(value))
         {
            throw new ArgumentOutOfRangeException("A bit can only be 0 or 1.", nameof(value));
         }

         this.Value = value;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalDigit" /> class 
      /// with its value set to the value of the parameter.
      /// </summary>
      /// <param name="value">Must represent an integer between 0 and 9.</param>
      public DecimalDigit(char value)
      {
         if (!DecimalDigit.CanConvert(value))
         {
            throw new ArgumentException("Argument does not represent a bit.", nameof(value));
         }

         this.Value = (int)value - 48; // 48 == ASCII '0'
      }
      #endregion

      #region Properties
      /// <summary>
      /// Gets the value of this digit as a character representation.
      /// </summary>
      public override char Numeral
      {
         get
         {
            return (char)(48 + this.Value); // 48 == ASCII for '0'
         }
      }

      /// <summary>
      /// Gets the value of this digit.
      /// </summary>
      protected override int Value { get; }
      #endregion

      #region Public Methods
      /// <summary>
      /// Verifies if the input can be converted to this digit type.
      /// </summary>
      /// <param name="input">A character possibly representing a decimal digit.</param>
      /// <returns>True if input can be used to create a decimal digit, else false.</returns>
      public static bool CanConvert(char input)
      {
         return AllowedValue.Contains(input);
      }

      /// <summary>
      /// Verifies if the input can be converted to this digit type.
      /// </summary>
      /// <param name="input">An integer possibly within the range of a decimal digit.</param>
      /// <returns>True if input can be used to create a decimal digit, else false.</returns>
      public static bool CanConvert(int input)
      {
         return AllowedValue.Contains((char)(input + 48));
      }
      #endregion
   }
}