﻿namespace Calculator.Base.Operations
{
   using System;
   using Digits;
   using Numbers;

   /// <summary>
   /// Represents the arithmetic operation: multiplication.
   /// </summary>
   public class Multiplication : Operation
   {
      /// <summary>
      /// The sign that symbolizes this operation.
      /// </summary>
      public override char Sign { get; } = '*';

      /// <summary>
      /// Determines whether this operation can be performed on the specified operands.
      /// Should always be called before actually performing the operation.
      /// </summary>
      /// <param name="left">The left-hand operand.</param>
      /// <param name="right">The right-hand operand.</param>
      /// <param name="message">Contains information why the operation cannot be performed.
      /// Is null if the operation can be performed.</param>
      /// <returns>
      ///   <c>true</c> if this operation can be performed on the operands; otherwise, <c>false</c>.
      /// </returns>
      public override bool CanPerformOn(BinaryNumber left, BinaryNumber right, out string message)
      {
         if (object.ReferenceEquals(left, null) || object.ReferenceEquals(left, null))
         {
            message = "None of the arguments must be null!";
            return false;
         }

         message = null;
         return true;
      }

      /// <summary>
      /// Multiplies both numbers.
      /// </summary>
      /// <param name="left">The left-hand side of the multiplication. Must not be null.</param>
      /// <param name="right">The right-hand side of the multiplication. Must not be null.</param>
      /// <returns>The product of both numbers.</returns>
      public override BinaryNumber PerformOn(BinaryNumber left, BinaryNumber right)
      {
         // Pass the larger number to the function as the first argument
         if (left >= right)
         {
            return this.Multiply(left, right);
         }
         else
         {
            return this.Multiply(right, left);
         }
      }

      /// <summary>
      /// The business logic for multiplying two binary numbers. Must not be called directly!
      /// </summary>
      /// <param name="left">The left-hand side of the multiplication. 
      /// Must not be less than the right-hand side.</param>
      /// <param name="right">The right-hand side of the multiplication.</param>
      /// <returns>The product of both numbers.</returns>
      private BinaryNumber Multiply(BinaryNumber left, BinaryNumber right)
      {
         // Special case
         // Multiplying by 0 results in 0
         if (left.Value == BinaryNumber.Zero || right.Value == BinaryNumber.Zero)
         {
            return new BinaryNumber(BinaryNumber.Zero);
         }

         // Setup
         var result = new BinaryNumber();
         var add = new Addition();

         /*For each digit of the smaller number, append a 0 to the result
         If that digit is a '1', add the larger number to the current result*/
         for (int i = 0; i < right.Length; ++i)
         {
            result = new BinaryNumber(result.Value + Bit.Zero);

            if (right[i].Numeral == Bit.One)
            {
               result = add.PerformOn(result, left);
            }
         }

         return result;
      }
   }
}
