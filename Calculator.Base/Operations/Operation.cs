﻿namespace Calculator.Base.Operations
{
   using Numbers;

   /// <summary>
   /// The base class for arithmetic operations.
   /// Extend this if operations on numbers of different types are necessary.
   /// </summary>
   public abstract class Operation
   {
      /// <summary>
      /// Gets the sign that symbolizes this operation.
      /// </summary>
      public abstract char Sign { get; }

      /// <summary>
      /// Performs this operation on the specified operands.
      /// CanPerformOn should be called prior to it.
      /// </summary>
      /// <param name="left">The left-hand side of the operation.</param>
      /// <param name="right">The right-hand side of the operation.</param>
      /// <returns>The result of the operation.</returns>
      public abstract BinaryNumber PerformOn(BinaryNumber left, BinaryNumber right);

      /// <summary>
      ///   Determines whether this operation can be performed on the specified operands.
      ///   Should always be called before actually performing the operation.
      /// </summary>
      /// <param name="left">The left-hand operand.</param>
      /// <param name="right">The right-hand operand.</param>
      /// <param name="message">
      ///   Contains information why the operation cannot be performed.
      ///   Is null if the operation can be performed.
      /// </param>
      /// <returns>
      ///   <c>true</c> if this operation can be performed on the operands; otherwise, <c>false</c>.
      /// </returns>
      public abstract bool CanPerformOn(BinaryNumber left, BinaryNumber right, out string message);
   }
}