﻿namespace Calculator.Base.Operations
{
   using System;
   using System.Collections.Generic;
   using Digits;
   using Numbers;

   /// <summary>
   /// Represents the arithmetic operation: subtraction.
   /// </summary>
   public class Subtraction : Operation
   {
      /// <summary>
      /// The sign that symbolizes this operation.
      /// </summary>
      public override char Sign { get; } = '-';

      /// <summary>
      /// Determines whether this operation can be performed on the specified operands.
      /// Should always be called before actually performing the operation.
      /// </summary>
      /// <param name="left">The left-hand operand.</param>
      /// <param name="right">The right-hand operand.</param>
      /// <param name="message">Contains information why the operation cannot be performed.
      /// Is null if the operation can be performed.</param>
      /// <returns>
      ///   <c>true</c> if this operation can be performed on the operands; otherwise, <c>false</c>.
      /// </returns>
      public override bool CanPerformOn(BinaryNumber left, BinaryNumber right, out string message)
      {
         if (object.ReferenceEquals(left, null) || object.ReferenceEquals(left, null))
         {
            message = "None of the arguments must be null!";
            return false;
         }

         if (left < right)
         {
            message = "Sorry, I can't handle negative results.";
            return false;
         }

         message = null;
         return true;
      }

      /// <summary>
      /// Subtracts the subtrahend from the minuend.
      /// </summary>
      /// <param name="minuend">The minuend. Must not be less than the subtrahend.</param>
      /// <param name="subtrahend">The subtrahend. Must not be greater than the minuend.</param>
      /// <returns>The difference between both numbers.</returns>
      public override BinaryNumber PerformOn(BinaryNumber minuend, BinaryNumber subtrahend)
      {
         return minuend == subtrahend ? new BinaryNumber() : this.Subtract(minuend, subtrahend);
      }

      /// <summary>
      /// The business logic for subtracting one binary number from another. 
      /// Must not be called directly!
      /// </summary>
      /// <param name="minuend">The minuend. Must not be less than the subtrahend.</param>
      /// <param name="subtrahend">The subtrahend. Must not be greater than the minuend.</param>
      /// <returns>The difference between both numbers.</returns>
      private BinaryNumber Subtract(BinaryNumber minuend, BinaryNumber subtrahend)
      {
         if (subtrahend.Value == BinaryNumber.Zero)
         {
            return minuend;
         }

         // Setup
         var sum = new Bit();
         var carry = new Bit();
         var result = new List<Bit>(minuend.Length); // The result can be as wide as the minuend
         int indexMinuend = minuend.Length - 1;
         int indexSubtrahend = subtrahend.Length - 1;

         /* Subtract the shorter number bitwise from the longer number, right to left
         The result will be filled in the wrong order. */
         for (; indexSubtrahend >= 0; --indexMinuend, --indexSubtrahend)
         {
            this.SubtractBit(
               minuend[indexMinuend],
               subtrahend[indexSubtrahend],
               carry,
               out carry,
               out sum);

            result.Add(sum);
         }

         // Compute the rest of the digits
         for (; indexMinuend >= 0; --indexMinuend)
         {
            this.SubtractBit(minuend[indexMinuend], carry, out carry, out sum);
            result.Add(sum);
         }

         result.Reverse();
         return new BinaryNumber(result);
      }

      /// <summary>
      /// Subtracts one bit from another.
      /// </summary>
      /// <param name="minuend">The minuend.</param>
      /// <param name="subtrahend">The subtrahend.</param>
      /// <param name="carry">The carry of the result. Only ever negative.</param>
      /// <param name="difference">The difference of the result.</param>
      private void SubtractBit(Bit minuend, Bit subtrahend, out Bit carry, out Bit difference)
      {
         this.SubtractBit(minuend, subtrahend, new Bit(), out carry, out difference);
      }

      /// <summary>
      /// Subtracts two bit from one bit.
      /// </summary>
      /// <param name="minuend">The minuend.</param>
      /// <param name="subtrahend1">The first subtrahend.</param>
      /// <param name="subtrahend2">The second subtrahend.</param>
      /// <param name="carry">The carry of the result. Only ever negative.</param>
      /// <param name="difference">The difference of the result.</param>
      private void SubtractBit(
         Bit minuend,
         Bit subtrahend1,
         Bit subtrahend2,
         out Bit carry,
         out Bit difference)
      {
         /*If 'subtrahend' and 'carry' are '1' OR 
         if 'minuend' is '0' and at least one of the other bits is '1'
         carry is '1'*/
         if ((subtrahend1.Numeral == Bit.One && subtrahend2.Numeral == Bit.One) ||
            (minuend.Numeral == Bit.Zero &&
               (subtrahend1.Numeral == Bit.One || subtrahend2.Numeral == Bit.One)))
         {
            // If either 'subtrahend' or 'carry' is '1', the difference is '1'
            difference = minuend ^ subtrahend1 ^ subtrahend2;
            carry = new Bit(Bit.One);
         }
         else
         {
            // If only 'minuend' is '1', the difference bit is '1'
            difference = minuend ^ subtrahend1 ^ subtrahend2;
            carry = new Bit(Bit.Zero);
         }
      }
   }
}
