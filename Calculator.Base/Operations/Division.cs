﻿namespace Calculator.Base.Operations
{
   using System;
   using System.Collections.Generic;
   using Digits;
   using Numbers;

   /// <summary>
   /// Represents the arithmetic operation: division.
   /// </summary>
   public class Division : Operation
   {
      /// <summary>
      /// The sign that symbolizes this operation.
      /// </summary>
      public override char Sign { get; } = '/';

      /// <summary>
      /// Determines whether this operation can be performed on the specified operands.
      /// Should always be called before actually performing the operation.
      /// </summary>
      /// <param name="left">The left-hand operand.</param>
      /// <param name="right">The right-hand operand.</param>
      /// <param name="message">
      /// Contains information why the operation cannot be performed.
      /// Is null if the operation can be performed.</param>
      /// <returns>
      ///   <c>true</c> if this operation can be performed on the operands; otherwise, <c>false</c>.
      /// </returns>
      public override bool CanPerformOn(BinaryNumber left, BinaryNumber right, out string message)
      {
         if (object.ReferenceEquals(left, null) || object.ReferenceEquals(left, null))
         {
            message = "None of the arguments must be null!";
            return false;
         }

         if (right.Value == BinaryNumber.Zero)
         {
            message = "Don't divide by zero.";
            return false;
         }

         message = null;
         return true;
      }

      /// <summary>
      /// Divides the dividend by the divisor.
      /// </summary>
      /// <param name="dividend">This number is divided by the divisor. Must not be null.</param>
      /// <param name="divisor">Divides the dividend. Must not be zero or null.</param>
      /// <returns>The ratio of both numbers.
      /// The Remainder of This will be set accordingly.</returns>
      public override BinaryNumber PerformOn(BinaryNumber dividend, BinaryNumber divisor)
      {
         var dummyRemainder = new BinaryNumber();
         return this.PerformOn(dividend, divisor, out dummyRemainder);
      }

      /// <summary>
      /// Performs the operation on the parameters.
      /// </summary>
      /// <param name="dividend">The dividend.</param>
      /// <param name="divisor">The divisor.</param>
      /// <param name="remainder">The remainder.</param>
      /// <returns>The result as a BinaryNumber.</returns>
      /// <exception cref="ArgumentNullException">
      /// None of the arguments must be null!
      /// </exception>
      /// <exception cref="System.ArgumentException">Don't divide by zero.</exception>
      public BinaryNumber PerformOn(
         BinaryNumber dividend,
         BinaryNumber divisor,
         out BinaryNumber remainder)
      {
         if (dividend.Value == BinaryNumber.Zero)
         {
            remainder = new BinaryNumber();
            return new BinaryNumber();
         }
         else if (dividend == divisor)
         {
            remainder = new BinaryNumber();
            return new BinaryNumber(BinaryNumber.One);
         }
         else if (dividend > divisor)
         {
            return this.Divide(dividend, divisor, out remainder);
         }
         else
         {
            remainder = dividend;
            return new BinaryNumber();
         }
      }

      /// <summary>
      /// The business logic for dividing two binary numbers. Must not be called directly!
      /// </summary>
      /// <param name="dividend">The dividend.</param>
      /// <param name="divisor">The divisor.</param>
      /// <param name="remainder">Will hold the remainder of the division.</param>
      /// <returns>The ratio of both numbers.</returns>
      private BinaryNumber Divide(BinaryNumber dividend, BinaryNumber divisor, out BinaryNumber remainder)
      {
         if (divisor.Value == BinaryNumber.One)
         {
            remainder = new BinaryNumber();
            return dividend;
         }

         // Setup
         int possibleSubtractions = (dividend.Length - divisor.Length) + 1;
         int indexDividend = divisor.Length - 1;
         /* One digit gets appended at the start of the loop, 
         therefor one less than the divisor's length is required here.*/
         var partForSubtraction = new BinaryNumber(dividend.Value.Substring(0, divisor.Length - 1));
         var ratio = new List<Bit>(possibleSubtractions);
         var sub = new Subtraction();

         /* First we check if the divisor can be subtracted from a part of the dividend.
         If so, a '1' is appended to the ratio and the subtraction carried out.
         Else a '0' is appended to the ratio.
         For the next subtraction, the difference of the previous subtraction is used and 
         the next digit of the dividend appended.*/
         for (int i = 0; i < possibleSubtractions; ++i)
         {
            partForSubtraction = new BinaryNumber(partForSubtraction.Value + dividend[indexDividend + i]);

            if (partForSubtraction >= divisor)
            {
               ratio.Add(new Bit(Bit.One));
               partForSubtraction = sub.PerformOn(partForSubtraction, divisor);
            }
            else
            {
               ratio.Add(new Bit());
            }
         }

         remainder = partForSubtraction;
         return new BinaryNumber(ratio);
      }
   }
}