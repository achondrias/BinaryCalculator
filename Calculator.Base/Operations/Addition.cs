﻿namespace Calculator.Base.Operations
{
   using System;
   using System.Collections.Generic;
   using Digits;
   using Numbers;

   /// <summary>
   /// Represents the arithmetic operation: addition.
   /// </summary>
   public class Addition : Operation
   {
      /// <summary>
      /// The sign that symbolizes this operation.
      /// </summary>
      public override char Sign { get; } = '+';

      /// <summary>
      /// Determines whether this operation can be performed on the specified operands.
      /// Should always be called before actually performing the operation.
      /// </summary>
      /// <param name="left">The left-hand operand.</param>
      /// <param name="right">The right-hand operand.</param>
      /// <param name="message">Contains information why the operation cannot be performed.
      /// Is null if the operation can be performed.</param>
      /// <returns>
      ///   <c>true</c> if this operation can be performed on the operands; otherwise, <c>false</c>.
      /// </returns>
      public override bool CanPerformOn(BinaryNumber left, BinaryNumber right, out string message)
      {
         if (object.ReferenceEquals(left, null) || object.ReferenceEquals(left, null))
         {
            message = "None of the arguments must be null!";
            return false;
         }

         message = null;
         return true;
      }

      /// <summary>
      /// Adds up both numbers.
      /// </summary>
      /// <param name="left">The left-hand side of the addition. Must not be null.</param>
      /// <param name="right">The right-hand side of the addition. Must not be null.</param>
      /// <returns>The sum of both numbers.</returns>
      /// <exception cref="ArgumentNullException">
      ///   None of the arguments must be null!
      /// </exception>
      public override BinaryNumber PerformOn(BinaryNumber left, BinaryNumber right)
      {
         // Pass the larger number to the function as the first argument
         if (left >= right)
         {
            return this.Add(left, right);
         }
         else
         {
            return this.Add(right, left);
         }
      }

      /// <summary>
      /// The business logic for adding two binary numbers. Must not be called directly!
      /// </summary>
      /// <param name="left">The left-hand side of the addition. 
      /// Must not be less than the right-hand side.</param>
      /// <param name="right">The right-hand side of the addition.</param>
      /// <returns>The sum of both numbers.</returns>
      private BinaryNumber Add(BinaryNumber left, BinaryNumber right)
      {
         // Special case
         // Either of the summands equal zero
         if (left.Value == BinaryNumber.Zero)
         {
            return right;
         }

         if (right.Value == BinaryNumber.Zero)
         {
            return left;
         }

         // Setup
         var sum = new Bit();
         var carry = new Bit();

         // The result can be one digit wider than the larger summand.
         var result = new List<Bit>(left.Length + 1);
         int indexLeft = left.Length - 1;
         int indexRight = right.Length - 1;

         /* Add the shorter number bitwise to the longer number, right to left
         The result will be filled in the wrong order. */
         for (; indexRight >= 0; --indexLeft, --indexRight)
         {
            this.AddBit(left[indexLeft], right[indexRight], carry, out carry, out sum);
            result.Add(sum);
         }

         // Compute the rest of the digits
         for (; indexLeft >= 0; --indexLeft)
         {
            this.AddBit(left[indexLeft], carry, out carry, out sum);
            result.Add(sum);
         }

         if (carry.Numeral == Bit.One)
         {
            result.Add(carry);
         }

         result.Reverse();
         return new BinaryNumber(result);
      }

      /// <summary>
      /// Adds up two bits.
      /// </summary>
      /// <param name="bit1">The first bit to add.</param>
      /// <param name="bit2">The second bit to add.</param>
      /// <param name="carry">The carry bit of the result.</param>
      /// <param name="sum">The sum bit of the result.</param>
      private void AddBit(Bit bit1, Bit bit2, out Bit carry, out Bit sum)
      {
         this.AddBit(bit1, bit2, new Bit(), out carry, out sum);
      }

      /// <summary>
      /// Adds up three bits.
      /// </summary>
      /// <param name="bit1">The first bit to add.</param>
      /// <param name="bit2">The second bit to add.</param>
      /// <param name="bit3">The third bit to add.</param>
      /// <param name="carry">The carry bit of the result.</param>
      /// <param name="sum">The sum bit of the result.</param>
      private void AddBit(Bit bit1, Bit bit2, Bit bit3, out Bit carry, out Bit sum)
      {
         // If at least two digits are '1', there is a carry
         if ((bit2.Numeral == Bit.One && bit3.Numeral == Bit.One) ||
            (bit1.Numeral == Bit.One &&
               (bit2.Numeral == Bit.One || bit3.Numeral == Bit.One)))
         {
            // If all three digits are '1', the sum bit is also '1'
            sum = bit1 ^ bit2 ^ bit3;
            carry = new Bit(Bit.One);
         }
         else
         {
            // If exactly one digit is '1', the sum bit is also '1'
            sum = bit1 ^ bit2 ^ bit3;
            carry = new Bit(Bit.Zero);
         }
      }
   }
}