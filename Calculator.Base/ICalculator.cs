﻿namespace Calculator.Base
{
   using System.Collections.Generic;

   /// <summary>
   /// Defines an extensible calculator for binary arithmetic.
   /// </summary>
   /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
   public interface ICalculator
   {
      /// <summary>
      /// Gets the available operations.
      /// </summary>
      /// <value>The available operations.</value>
      IEnumerable<char> AvailableOperations { get; }

      /// <summary>
      /// Performs a calculation by applying the operator on both operands.
      /// Make sure to call the CanCalculate method prior to calling this method!
      /// Else undefined behavior will result.
      /// </summary>
      /// <param name="operatorSign">The operator sign.</param>
      /// <param name="leftOperand">The left operand.</param>
      /// <param name="rightOperand">The right operand.</param>
      /// <returns>The result of the calculation.</returns>
      string Calculate(
         char operatorSign,
         string leftOperand, 
         string rightOperand);

      /// <summary>
      /// Determines whether this instance can perform a calculation on the specified parameters.
      /// </summary>
      /// <param name="operatorSign">The operator sign.</param>
      /// <param name="leftOperand">The left operand.</param>
      /// <param name="rightOperand">The right operand.</param>
      /// <param name="message">
      ///   Contains information why the calculation cannot be performed.
      ///   Is null if the calculation can be performed.
      /// </param>
      /// <returns>
      ///   <c>true</c> if this instance can perform a calculation; otherwise, <c>false</c>.
      /// </returns>
      bool CanCalculate(
         char operatorSign,
         string leftOperand,
         string rightOperand,
         out string message);
   }
}