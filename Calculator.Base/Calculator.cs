﻿namespace Calculator.Base
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using Numbers;
   using Operations;

   /// <summary>
   /// An extensible calculator that operates on binary numbers.
   /// </summary>
   public sealed class Calculator : ICalculator
   {
      /// <summary>
      /// The available operations for this calculator. Set during construction.
      /// </summary>
      private IEnumerable<Operation> availableOperations;

      /// <summary>
      /// The available operator signs. 
      /// </summary>
      private IEnumerable<char> availableOperatorSigns;

      /// <summary>
      /// Initializes a new instance of the <see cref="Calculator" /> class, 
      /// offering functionality depending on the given operations.
      /// Its initial operator is set to the first operation of the parameter.
      /// Its initial operands are zero as well as its result.
      /// </summary>
      /// <param name="operations">The functionality this calculator will offer.
      /// Must not be null, else an ArgumentNullException is thrown.</param>
      public Calculator(IEnumerable<Operation> operations)
      {
         if (operations == null)
         {
            throw new ArgumentNullException($"{nameof(operations)} must not be null.");
         }

         if (operations.Count() < 1)
         {
            throw new ArgumentException($"{nameof(operations)} must not be empty.");
         }

         this.availableOperations = operations;
         this.availableOperatorSigns = operations.Select(op => op.Sign);
      }

      /// <summary>
      /// Gets the available operations.
      /// </summary>
      /// <value>The available operations.</value>
      public IEnumerable<char> AvailableOperations
      {
         get
         {
            return this.availableOperatorSigns;
         }
      }

      /// <summary>
      /// Determines whether this instance can perform a calculation on the specified parameters.
      /// </summary>
      /// <param name="operatorSign">
      /// The operator sign. Must be part of the AvailableOperations.</param>
      /// <param name="leftOperand">The left operand. Must consist of ones and zeroes only.
      /// </param>
      /// <param name="rightOperand">
      /// The right operand. Must consist of ones and zeros only.
      /// </param>
      /// <param name="message">Contains information why the calculation cannot be performed.
      /// Is null if the calculation can be performed.</param>
      /// <returns>
      ///   <c>true</c> if this instance can perform a calculation; otherwise, <c>false</c>.
      /// </returns>
      public bool CanCalculate(
         char operatorSign,
         string leftOperand,
         string rightOperand,
         out string message)
      {
         string messageLeftOperand, messageRightOperand;
         bool operatorIsValid = this.availableOperations.Any(op => op.Sign == operatorSign);
         bool leftIsValid = BinaryNumber.CanConvert(leftOperand, out messageLeftOperand);
         bool rightIsValid = BinaryNumber.CanConvert(rightOperand, out messageRightOperand);
         Operation specifiedOperation = this.availableOperations.First(op => op.Sign == operatorSign);
         BinaryNumber lefthandNumber, righthandNumber;

         if (!operatorIsValid)
         {
            message = "No operation with sign " + operatorSign + " known.";
            return false;
         }

         if (!(leftIsValid || rightIsValid))
         {
            message = "Both operands are invalid. " + 
               $"Left: {messageLeftOperand} " + 
               $"Right: {messageRightOperand}";
            return false;
         }

         if (!leftIsValid)
         {
            message = "Left operand invalid. " + messageLeftOperand;
            return false;
         }

         if (!rightIsValid)
         {
            message = "Right operand invalid. " + messageRightOperand;
            return false; 
         }

         lefthandNumber = new BinaryNumber(leftOperand);
         righthandNumber = new BinaryNumber(rightOperand);

         return specifiedOperation.CanPerformOn(lefthandNumber, righthandNumber, out message);
      }

      /// <summary>
      /// Performs a calculation by applying the operator on both operands.
      /// Make sure to call the CanCalculate method prior to calling this method!
      /// Else undefined behavior will result.
      /// </summary>
      /// <param name="operatorSign">The operator sign.</param>
      /// <param name="leftOperand">The left operand.</param>
      /// <param name="rightOperand">The right operand.</param>
      /// <returns>
      /// The result of the calculation.
      /// </returns>
      public string Calculate(char operatorSign, string leftOperand, string rightOperand)
      {
         var lefthandNumber = new BinaryNumber(leftOperand);
         var righthandNumber = new BinaryNumber(rightOperand);
         Operation specifiedOperation = this.availableOperations.First(op => op.Sign == operatorSign);

         return specifiedOperation.PerformOn(lefthandNumber, righthandNumber).ToString();
      }
   }
}