﻿namespace Calculator.Base.Numbers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using Digits;

   /// <summary>
   /// Represents a binary number. The value of each digit lies in the range of 0 - 1.
   /// </summary>
   public class BinaryNumber : Number<Bit>
   {
      #region Fields
      /// <summary>
      /// The string representation of the value zero of a binary number.
      /// </summary>
      public static readonly string Zero = "0";

      /// <summary>
      /// The string representation of the value one of a binary number.
      /// </summary>
      public static readonly string One = "1";
      #endregion

      #region Constructors
      /// <summary>
      /// Initializes a new instance of the <see cref="BinaryNumber" /> class 
      /// with its value set to zero.
      /// </summary>
      public BinaryNumber()
      {
         this.Digits = new List<Bit> { new Bit() };
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="BinaryNumber" /> class
      /// with its value set to the value represented by the parameter.
      /// Make sure to call the CanConvert method prior to initialization.
      /// </summary>
      /// <param name="value">Must represent a binary number. Leading zeros will be trimmed.</param>
      public BinaryNumber(string value)
      {
         value = value.TrimStart(Bit.Zero);
         this.Digits = value.Length == 0 ? new List<Bit>() { new Bit() } :
            value.Select(c => new Bit(c));
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="BinaryNumber" /> class 
      /// with its value set to the value represented by the parameter.
      /// </summary>
      /// <param name="value">Leading zeros will be trimmed.</param>
      public BinaryNumber(IEnumerable<Bit> value)
      {
         // Trim leading zeros.
         value = value.SkipWhile(bit => bit.Numeral == Bit.Zero);

         // If the trimming erased everything, set the value to zero.
         this.Digits = value.Count() == 0 ? new List<Bit> { new Bit() } : value;
      }
      #endregion

      #region Properties
      /// <summary>
      /// Gets the value of this number as a string representation.
      /// </summary>
      public override string Value
      {
         get { return this.ToString(); }
      }

      /// <summary>
      /// Gets the value of this number as a collection of digits.
      /// </summary>
      protected override IEnumerable<Bit> Digits { get; }
      #endregion

      #region Public Methods
      /// <summary>
      /// Verifies if the input can be converted to this number type.
      /// </summary>
      /// <param name="input">A string possibly representing a binary number.</param>
      /// <returns>True if input can be used to create a binary number, else false.</returns>
      public static bool CanConvert(string input, out string message)
      {
         var isConvertable = true;

         if (input == null)
         {
            message = "Null cannot be converted.";
            return false;
         }

         isConvertable = !input.Any(character => !Bit.CanConvert(character));

         if (!isConvertable)
         {
            message = "Operands must consist of ones and zeros only.";
            return false;
         }

         message = null;
         return true;
      }

      /// <summary>
      /// Returns a string representation of this number.
      /// </summary>
      /// <returns>A string representing this number.</returns>
      public override string ToString()
      {
         var builder = new StringBuilder(this.Digits.Count());

         foreach (Bit b in this.Digits)
         {
            builder.Append(b);
         }

         return builder.ToString();
      }
      #endregion
   }
}