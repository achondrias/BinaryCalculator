﻿namespace Calculator.Base.Numbers
{
   using System;
   using System.Collections;
   using System.Collections.Generic;
   using System.Linq;
   using Digits;

   /// <summary>
   /// Base class for numbers. A number is thought of as a collection of digits.
   /// </summary>
   /// <typeparam name="T">The type of digit this number is a collection of.
   /// Is equal to the numeral system since it defines the range of allowed values.</typeparam>
   /// <seealso cref="System.IEquatable{Model.Numbers.Number{T}}" />
   /// <seealso cref="System.IComparable{Model.Numbers.Number{T}}" />
   /// <seealso cref="System.Collections.Generic.IEnumerable{T}" />
   public abstract class Number<T> : IEquatable<Number<T>>, IComparable<Number<T>>, IEnumerable<T>
      where T : Digit
   {
      #region Properties
      /// <summary>
      /// Gets the value of this number as a string representation.
      /// </summary>
      /// <value>The value.</value>
      public abstract string Value { get; }

      /// <summary>
      /// Gets the number's amount of digits.
      /// </summary>
      /// <value>The length.</value>
      public int Length
      {
         get { return this.Digits.Count(); }
      }

      /// <summary>
      /// Gets the value of this number as a collection of digits.
      /// </summary>
      /// <value>The digits.</value>
      protected abstract IEnumerable<T> Digits { get; }
      #endregion

      #region Indexer
      /// <summary>
      /// Gets the digit at the given index.
      /// </summary>
      /// <param name="key">The key.</param>
      /// <returns>The digit at the specified position.</returns>
      public T this[int key] { get { return this.Digits.ElementAt(key); } }
      #endregion

      #region Operators
      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if both numbers have the same value, else false.</returns>
      public static bool operator ==(Number<T> left, Number<T> right)
      {
         return left.Equals(right);
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if both numbers have a different value, else false.</returns>
      public static bool operator !=(Number<T> left, Number<T> right)
      {
         return !(left == right);
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side number is less than
      /// the right-hand side number.</returns>
      public static bool operator <(Number<T> left, Number<T> right)
      {
         return left.CompareTo(right) < 0 ? true : false;
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side number is greater than
      /// the right-hand side number.</returns>
      public static bool operator >(Number<T> left, Number<T> right)
      {
         return left.CompareTo(right) > 0 ? true : false;
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side number is not greater than
      /// the right-hand side number.</returns>
      public static bool operator <=(Number<T> left, Number<T> right)
      {
         return left.CompareTo(right) < 1 ? true : false;
      }

      /// <summary>
      /// Compares the values of both numbers.
      /// </summary>
      /// <param name="left">The first comparand.</param>
      /// <param name="right">The second comparand.</param>
      /// <returns>True if the left-hand side number is not less than
      /// the right-hand side number.</returns>
      public static bool operator >=(Number<T> left, Number<T> right)
      {
         return left.CompareTo(right) > -1 ? true : false;
      }

      #endregion

      #region Public Methods
      /// <summary>
      /// Compares this to <paramref name="other"/>.
      /// </summary>
      /// <param name="other">The comparand.</param>
      /// <returns>1 if this is larger.
      /// 0 if both are equal.
      /// -1 if this is less.</returns>
      /// <exception cref="ArgumentException">
      /// The digits of this number must not be empty!
      /// or
      /// The digits of the comparand must not be empty!
      /// </exception>
      public virtual int CompareTo(Number<T> other)
      {
         if (!this.Digits.Any())
         {
            throw new ArgumentException("The digits of this number must not be empty!");
         }

         if (!other.Digits.Any())
         {
            throw new ArgumentException("The digits of the comparand must not be empty!");
         }

         // The longer number is larger.
         if (this.Digits.Count() > other.Digits.Count())
         {
            return 1;
         }

         if (this.Digits.Count() < other.Digits.Count())
         {
            return -1;
         }

         // When both have the same size, compare them digit by digit.
         for (int i = 0; i < this.Digits.Count(); i++)
         {
            if (this.Digits.ElementAt(i) > other.Digits.ElementAt(i))
            {
               return 1;
            }
            else if (this.Digits.ElementAt(i) < other.Digits.ElementAt(i))
            {
               return -1;
            }
         }

         return 0;
      }

      /// <summary>
      /// Compares the values of this and the given parameter.
      /// </summary>
      /// <param name="obj">The object to compare with.</param>
      /// <returns>False if object either is not of the same type
      /// or has a different value. Else true.</returns>
      public override bool Equals(object obj)
      {
         return this.GetType() == obj.GetType() ? this.Equals((Number<T>)obj) : false;
      }

      /// <summary>
      /// Compares the values of this and the given parameter.
      /// </summary>
      /// <param name="other">The number to compare with.</param>
      /// <returns>True if both have the same value. Else false.</returns>
      public bool Equals(Number<T> other)
      {
         if (object.ReferenceEquals(other, null))
         {
            return object.ReferenceEquals(this, null);
         }

         if (this.GetType() != other.GetType())
         {
            return false;
         }

         if (this.Digits.Count() != other.Digits.Count())
         {
            return false;
         }
         else
         {
            for (int i = 0; i < this.Digits.Count(); i++)
            {
               if (this.Digits.ElementAt(i) != other.Digits.ElementAt(i))
               {
                  return false;
               }
            }

            return true;
         }
      }

      /// <summary>
      /// Computes a hash code for this number based on its value.
      /// </summary>
      /// <returns>A hash code for this number.</returns>
      public override int GetHashCode()
      {
         return this.Value.GetHashCode();
      }

      /// <summary>
      /// Returns a string representation of this number.
      /// </summary>
      /// <returns>A string representing this number.</returns>
      public abstract override string ToString();

      /// <summary>
      /// Gets the enumerator.
      /// </summary>
      /// <returns>IEnumerator&lt;T&gt; of <paramref name="Digits"/>.</returns>
      public IEnumerator<T> GetEnumerator()
      {
         return this.Digits.GetEnumerator();
      }

      /// <summary>
      /// Gets the enumerator.
      /// </summary>
      /// <returns>IEnumerator of <paramref name="Digits"/>.</returns>
      IEnumerator IEnumerable.GetEnumerator()
      {
         return this.Digits.GetEnumerator();
      }
      #endregion
   }
}