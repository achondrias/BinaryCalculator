﻿namespace Calculator.Base.Numbers
{
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using Digits;

   /// <summary>
   /// Represents a decimal number. The value of each digit lies in the range of 0 - 9.
   /// </summary>
   public class DecimalNumber : Number<DecimalDigit>
   {
      #region Fields
      /// <summary>
      /// A decimal number with a value of zero. Used for comparisons.
      /// </summary>
      public static readonly DecimalNumber Zero = new DecimalNumber();
      #endregion

      #region Constructors
      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalNumber" /> class 
      /// with its value set to zero.
      /// </summary>
      public DecimalNumber()
      {
         this.Digits = new List<DecimalDigit> { new DecimalDigit() };
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalNumber" /> class 
      /// with its value set to the value represented by the parameter.
      /// </summary>
      /// <param name="value">Must represent a binary number, else an ArgumentException is thrown.
      /// Leading zeros will be trimmed.</param>
      public DecimalNumber(string value)
      {
         if (!DecimalNumber.CanConvert(value))
         {
            throw new System.ArgumentException(
               "The provided argument does not represent a decimal number",
               nameof(value));
         }

         value = value.TrimStart(DecimalDigit.Zero);
         this.Digits = value.Length == 0 ? DecimalNumber.Zero.Digits : 
            value.Select(c => new DecimalDigit(c));
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DecimalNumber"/> class.
      /// </summary>
      /// <param name="value">The value of the number.</param>
      public DecimalNumber(IEnumerable<DecimalDigit> value)
      {
         // Trim leading zeros.
         value = value.SkipWhile(digit => digit.Numeral == DecimalDigit.Zero);

         // If the trimming erased everything, set the value to zero.
         this.Digits = value.Count() == 0 ? new List<DecimalDigit> { new DecimalDigit() } : value;
      }
      #endregion

      #region Properties
      /// <summary>
      /// Gets the value of this number as a collection of digits.
      /// </summary>
      public override string Value
      {
         get { return this.ToString(); }
      }

      /// <summary>
      /// The value of this number as a collection of digits.
      /// </summary>
      protected override IEnumerable<DecimalDigit> Digits { get; }
      #endregion

      #region Public Methods
      /// <summary>
      /// Verifies if the input can be converted to this number type.
      /// </summary>
      /// <param name="input">A string possibly representing a decimal number.</param>
      /// <returns>True if input can be used to create a decimal number, else false.</returns>
      public static bool CanConvert(string input)
      {
         foreach (char c in input)
         {
            if (!DecimalDigit.CanConvert(c))
            {
               return false;
            }
         }

         return true;
      }

      /// <summary>
      /// Returns a string representation of this number.
      /// </summary>
      /// <returns>A string representing this number.</returns>
      public override string ToString()
      {
         var builder = new StringBuilder();

         foreach (DecimalDigit d in this.Digits)
         {
            builder.Append(d);
         }

         return builder.ToString();
      }
      #endregion
   }
}