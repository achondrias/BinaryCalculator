﻿namespace Calculator.Console.Output
{
   using System;
   using System.Text;

   /// <summary>
   /// This class provides functionality related to displaying information to the user.
   /// </summary>
   internal static class Display
   {
      /// <summary>
      /// Writes the continue message to the Console.
      /// </summary>
      public static void Continue()
      {
         System.Console.WriteLine(Messages.ContinueWithEnter);
      }

      /// <summary>
      /// Writes an error message with the provided error message to the Console.
      /// </summary>
      /// <param name="errorMessage">The error message to show.</param>
      public static void Error(string errorMessage)
      {
         System.Console.Clear();
         System.Console.WriteLine(Errors.ErrorHeader);
         System.Console.WriteLine();
         System.Console.WriteLine(errorMessage);
         System.Console.WriteLine();
         System.Console.WriteLine(Messages.ContinueWithEnter);
      }

      /// <summary>
      /// Writes the goodbye message to the Console.
      /// </summary>
      public static void Goodbye()
      {
         System.Console.WriteLine(Messages.Bye);
      }

      /// <summary>
      /// Displays the main menu.
      /// </summary>
      public static void MainMenu()
      {
         System.Console.WriteLine(Messages.MainmenuHeader);
      }

      /// <summary>
      /// Writes the binary number to the Console.
      /// </summary>
      /// <param name="input">The number to write.</param>
      public static void PrintBinaryNumber(string input)
      {
         // Setup
         char separator = ' ';
         uint interval = 4;

         System.Console.WriteLine("Binary Value:");
         System.Console.WriteLine(Display.Separate(input, separator, interval));
         System.Console.WriteLine();
      }

      /// <summary>
      /// Writes the decimal number to the Console.
      /// </summary>
      /// <param name="input">The number to write.</param>
      public static void PrintDecimalNumber(string input)
      {
         // Setup
         char separator = '.';
         uint interval = 3;

         System.Console.WriteLine("Decimal Value:");
         System.Console.WriteLine(Display.Separate(input, separator, interval));
         System.Console.WriteLine();
      }

      /// <summary>
      /// Displays a result screen, showing it once as a binary and once as a decimal number.
      /// </summary>
      /// <param name="binaryResult">The result as a binary number. Should be equal to the decimal number.</param>
      /// <param name="decimalResult">The result as a decimal number. Should be equal to the binary number.</param>
      public static void Result(string binaryResult, string decimalResult)
      {
         System.Console.Clear();
         Display.PrintBinaryNumber(binaryResult);
         Display.PrintDecimalNumber(decimalResult);
         System.Console.WriteLine();
         Display.Continue();
      }

      /// <summary>
      /// Inserts the separator into the input after each step defined by interval.
      /// It goes right to left, such that the first separator might be earlier than the given interval.
      /// </summary>
      /// <param name="input">The string to put the separators into. Must not be null or empty.</param>
      /// <param name="separator">The separator to insert into the string.</param>
      /// <param name="interval">Defines after how many characters a separator gets inserted. Must not be zero.</param>
      /// <returns>A new string with the inserted separators.</returns>
      private static string Separate(string input, char separator, uint interval)
      {
         if (string.IsNullOrEmpty(input) || interval == 0)
         {
            throw new System.ArgumentNullException(Errors.SeparateArguments);
         }

         // Setup
         int lastInputIndex = input.Length - 1;
         var reverseResultBuilder = new StringBuilder();
         char[] result;

         // Append the first character, else a separator is always added as a first character.
         reverseResultBuilder.Append(input[lastInputIndex]);

         // Append every character and, if necessary, append a separator
         for (int i = lastInputIndex - 1; i >= 0; --i)
         {
            if ((lastInputIndex - i) % interval == 0)
            {
               reverseResultBuilder.Append(separator);
            }

            reverseResultBuilder.Append(input[i]);
         }

         /*We appended in reverse order to have the first block of characters with the correct length
         Therefor we now have to reverse it*/
         result = reverseResultBuilder.ToString().ToCharArray();
         Array.Reverse(result);

         return new string(result);
      }
   }
}
