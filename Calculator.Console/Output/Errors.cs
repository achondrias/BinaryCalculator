﻿namespace Calculator.Console.Output
{
   /// <summary>
   /// A collection of error messages for the user.
   /// </summary>
   public static class Errors
   {
      /// <summary>
      /// The cannot convert to binary message.
      /// </summary>
      public static readonly string CannotConvertToBinary = "Cannot convert the argument to a binary number.";

      /// <summary>
      /// The cannot convert to decimal message.
      /// </summary>
      public static readonly string CannotConvertToDecimal = "Cannot convert the argument to a decimal number.";

      /// <summary>
      /// The command constructor error message.
      /// </summary>
      public static readonly string CommandConstructorError = "Cannot build an object of type Command with the given arguments.";

      /// <summary>
      /// The command cannot be executed message.
      /// </summary>
      public static readonly string CommandCannotBeExecuted = "This command could not be executed.";

      /// <summary>
      /// The divide by zero message.
      /// </summary>
      public static readonly string DivideByZero = "Division by 0 is not allowed.";

      /// <summary>
      /// The divisor larger than dividend message.
      /// </summary>
      public static readonly string DivisorLargerThanDividend = "The divisor must be smaller than the dividend.";

      /// <summary>
      /// The error header message.
      /// </summary>
      public static readonly string ErrorHeader = "\t\tERROR";

      /// <summary>
      /// The generic error message.
      /// </summary>
      public static readonly string GenericError = "Something somewhere went wrong for some reason.";

      /// <summary>
      /// The invalid choice message.
      /// </summary>
      public static readonly string InvalidChoice = "Your choice was not a valid one.";

      /// <summary>
      /// The negative result message.
      /// </summary>
      public static readonly string NegativeResult = "Sorry, I can't handle negative results :(";

      /// <summary>
      /// The not a number message.
      /// </summary>
      public static readonly string NotANumber = "You did not enter a number.";

      /// <summary>
      /// The not only digits message.
      /// </summary>
      public static readonly string NotOnlyDigits = "There must only be 1's and 0's!";

      /// <summary>
      /// The separate arguments message.
      /// </summary>
      public static readonly string SeparateArguments = "Either the string was empty/null or the separator set to zero.";
   }
}
