﻿namespace Calculator.Console.Output
{
   /// <summary>
   /// A collection of messages for the user.
   /// </summary>
   internal static class Messages
   {
      /// <summary>
      /// The bye message
      /// </summary>
      public static readonly string Bye =
         "Good night, sweet prince.";

      /// <summary>
      /// The continue with enter message.
      /// </summary>
      public static readonly string ContinueWithEnter =
         "To continue, press Enter.";

      /// <summary>
      /// The main menu header message.
      /// </summary>
      public static readonly string MainmenuHeader =
         "\t\tBINARY CALCULATOR 3000\n";

      /// <summary>
      /// The input prompt message.
      /// </summary>
      public static readonly string InputPrompt =
         @"Please enter the operation you want to perform in the following manner:
Operand Operator Operand.
E.g. 101 + 1

To quit, just enter 'end'.";

      /// <summary>
      /// The tests passed successfully message.
      /// </summary>
      public static readonly string TestsPassedSuccessfully =
         "Tests passed successfully. Praise the sun \\[+]/";
   }
}
