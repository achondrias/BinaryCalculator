﻿namespace Calculator.Console
{
   using System.Collections.Generic;
   using Base;
   using Base.Operations;
   using ValueConverter;

   /// <summary>
   /// The main program. Does the set- and cleanup the application requires.
   /// </summary>
   public class Program
   {
      /// <summary>
      /// The entry point.
      /// </summary>
      /// <param name="args">The arguments.</param>
      public static void Main(string[] args)
      {
         var operations = new List<Operation>()
         {
            new Addition(),
            new Multiplication(),
            new Subtraction(),
            new Division()
         };

         Application.Loop(new Calculator(operations), new BinaryDecimalConverter());
      }
   }
}
