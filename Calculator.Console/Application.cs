﻿namespace Calculator.Console
{
   using Base;
   using Data;
   using Output;
   using ValueConverter;

   /// <summary>
   /// The application.
   /// </summary>
   internal static class Application
   {
      /// <summary>
      /// The main loop of the application.
      /// </summary>
      /// <param name="calculator">The calculator to use for calculations.</param>
      /// <param name="converter">The converter to use for number conversions.</param>
      /// <returns>True when it finishes.</returns>
      public static bool Loop(ICalculator calculator, INumberConverter converter)
      {
         string binaryResult;
         string decimalResult;
         Command calculation;
         string errorMessage;
         string userInput;
         bool keepLooping = true;

         while (keepLooping)
         {
            System.Console.Clear();
            Display.MainMenu();
            System.Console.WriteLine(Messages.InputPrompt);
            userInput = System.Console.ReadLine();

            if (Command.IsQuitCommand(userInput))
            {
               keepLooping = false;
               continue;
            }
            else if (!Command.TryParse(userInput, calculator.AvailableOperations, out calculation))
            {
               Display.Error(Errors.InvalidChoice);
               System.Console.ReadLine();
               continue;
            }
            else
            {
               if (calculator.CanCalculate(
                     calculation.Operation,
                     calculation.FirstOperand,
                     calculation.SecondOperand,
                     out errorMessage))
               {
                  binaryResult = calculator.Calculate(
                     calculation.Operation,
                     calculation.FirstOperand,
                     calculation.SecondOperand);
               }
               else
               {
                  Display.Error(errorMessage);
                  System.Console.ReadLine();
                  continue;
               }

               decimalResult = converter.ConvertBinaryToDecimal(binaryResult);
               Display.Result(binaryResult, decimalResult);
               System.Console.ReadLine();
            }
         }

         System.Console.Clear();
         Display.Goodbye();
         System.Console.ReadLine();

         return true;
      }
   }
}