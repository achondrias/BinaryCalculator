﻿namespace Calculator.Console.Data
{
   using System;
   using System.Collections.Generic;
   using System.Linq;

   /// <summary>
   /// A command representing a binary calculation. Consists of two operands and an operator.
   /// Can be used to pass the calculation to a method that can calculate the result.
   /// </summary>
   internal class Command
   {
      /// <summary>
      /// The quit command. Can be used by the user to request a shutdown of the program.
      /// </summary>
      private static readonly string QuitCommand = "end";

      /// <summary>
      /// The binary digits. Used to check if a string can be used as an operand.
      /// </summary>
      private static readonly string BinaryDigits = "01";

      /// <summary>
      /// The number zero. Used as a default value for operands.
      /// </summary>
      private static readonly string Zero = "0";

      /// <summary>
      /// Initializes a new instance of the <see cref="Command"/> class. 
      /// </summary>
      /// <param name="operatorSign">The operator sign.</param>
      /// <param name="leftOperand">The left operand.</param>
      /// <param name="rightOperand">The right operand.</param>
      private Command(char operatorSign, string leftOperand, string rightOperand)
      {
         this.Operation = operatorSign;
         this.FirstOperand = string.IsNullOrEmpty(leftOperand) ? Command.Zero : leftOperand;
         this.SecondOperand = string.IsNullOrEmpty(rightOperand) ? Command.Zero : rightOperand;
      }

      /// <summary>
      /// Gets the first operand.
      /// </summary>
      /// <value>
      /// The first operand.
      /// </value>
      public string FirstOperand { get; }

      /// <summary>
      /// Gets the second operand.
      /// </summary>
      /// <value>
      /// The second operand.
      /// </value>
      public string SecondOperand { get; }

      /// <summary>
      /// Gets the operation.
      /// </summary>
      /// <value>
      /// The operation.
      /// </value>
      public char Operation { get; }

      /// <summary>
      /// Converts the string representation of a binary calculation to its command equivalent. 
      /// A return value indicates whether the operation succeeded.
      /// </summary>
      /// <param name="input">The binary calculation.</param>
      /// <param name="allowedOperatorSigns">
      /// All operator signs that are allowed to appear in the calculation.
      /// </param>
      /// <param name="output">
      /// When this method returns, contains the command equivalent of the binary calculation 
      /// contained in <paramref name="input"/>
      /// </param>
      /// <returns>
      /// <c>True</c> if <paramref name="input"/> was converted successfully; otherwise <c>false</c>.
      /// </returns>
      public static bool TryParse(
         string input,
         IEnumerable<char> allowedOperatorSigns,
         out Command output)
      {
         if (string.IsNullOrEmpty(input))
         {
            output = null;
            return false;
         }

         bool hasValidOperator, hasValidLeftOperand, hasValidRightOperand;
         char operatorSign = char.MinValue;
         string leftOperand = string.Empty;
         string rightOperand = string.Empty;
         string allowedChars = " " + Command.BinaryDigits;
         int amountOfOperands = 2;
         string[] inputParts;

         inputParts = input.Split(
            allowedOperatorSigns.ToArray(),
            amountOfOperands);

         if (inputParts.Length == amountOfOperands)
         {
            leftOperand = inputParts[0];
            rightOperand = inputParts[1];
            leftOperand = leftOperand.Trim();
            rightOperand = rightOperand.Trim();

            hasValidLeftOperand = leftOperand.All(c => allowedChars.Contains(c));
            hasValidRightOperand = rightOperand.All(c => allowedChars.Contains(c));
         }
         else
         {
            hasValidLeftOperand = false;
            hasValidRightOperand = false;
         }

         try
         {
            operatorSign = input.Single(c => allowedOperatorSigns.Contains(c));
            hasValidOperator = true;
         }
         catch (InvalidOperationException)
         {
            hasValidOperator = false;
         }

         if (hasValidOperator && hasValidLeftOperand && hasValidRightOperand)
         {
            output = new Command(operatorSign, leftOperand, rightOperand);
            return true;
         }
         else
         {
            output = null;
            return false;
         }
      }

      /// <summary>
      /// Checks if the input equals the command to quit the program.
      /// </summary>
      /// <param name="input">The string to check.</param>
      /// <returns>True if it is equal to the quit command, else false.</returns>
      public static bool IsQuitCommand(string input)
      {
         return (input.ToLowerInvariant() == Command.QuitCommand) ? true : false;
      }
   }
}
