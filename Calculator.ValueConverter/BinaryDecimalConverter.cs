﻿namespace Calculator.ValueConverter
{
   using System.Collections.Generic;
   using Base.Digits;
   using Base.Numbers;
   using Base.Operations;

   public class BinaryDecimalConverter : INumberConverter
   {
      /// <summary>
      /// The conversion table mapping all decimal digits to their binary representation.
      /// </summary>
      private readonly Dictionary<BinaryNumber, DecimalDigit> conversionTable =
         new Dictionary<BinaryNumber, DecimalDigit>()
         {
            { new BinaryNumber("0"), new DecimalDigit('0') },
            { new BinaryNumber("1"), new DecimalDigit('1') },
            { new BinaryNumber("10"), new DecimalDigit('2') },
            { new BinaryNumber("11"), new DecimalDigit('3') },
            { new BinaryNumber("100"), new DecimalDigit('4') },
            { new BinaryNumber("101"), new DecimalDigit('5') },
            { new BinaryNumber("110"), new DecimalDigit('6') },
            { new BinaryNumber("111"), new DecimalDigit('7') },
            { new BinaryNumber("1000"), new DecimalDigit('8') },
            { new BinaryNumber("1001"), new DecimalDigit('9') }
         };
      
      /// <summary>
      /// The division used during conversion.
      /// </summary>
      private Division division = new Division();

      /// <summary>
      /// Determines whether this instance can convert the specified input
      /// from a binary to a decimal number.
      /// </summary>
      /// <param name="input">The input. If it equals a binary number, it can be converted.</param>
      /// <returns>
      ///   <c>true</c> if this instance can convert the specified input; otherwise, <c>false</c>.
      /// </returns>
      /// <exception cref="NotImplementedException"></exception>
      public bool CanConvertBinaryToDecimal(string input)
      {
         string message;

         return BinaryNumber.CanConvert(input, out message);
      }

      /// <summary>
      /// Converts the input to its decimal representation.
      /// Make sure conversion is possible by calling the corresponding CanConvert method.
      /// </summary>
      /// <param name="input">The binary number to convert.</param>
      /// <returns>
      /// The input number in its decimal representation.
      /// </returns>
      public string ConvertBinaryToDecimal(string input)
      {
         if (input == null)
         {
            return string.Empty;
         }

         // Setup
         var currentNumber = new BinaryNumber(input as string);
         int requiredSize = (currentNumber.Length / 3) + 1;
         var result = new List<DecimalDigit>(requiredSize);
         BinaryNumber remainder;
         var ten = new BinaryNumber("1010");

         /*Divide the currentNumber by '1010' (10 as a decimal) while it is larger than '1010'.
         The remainder of each division equals the value of a digit of the decimal.*/
         while (!this.conversionTable.ContainsKey(currentNumber))
         {
            currentNumber = this.division.PerformOn(currentNumber, ten, out remainder);
            result.Add(this.conversionTable[remainder]);
         }

         // What remains equals the leftmost digit of the decimal
         result.Add(this.conversionTable[currentNumber]);
         result.Reverse();

         return new DecimalNumber(result).ToString();
      }
   }
}
