﻿namespace Calculator.ValueConverter
{
   /// <summary>
   /// Defines a converter that converts from one number type to another.
   /// </summary>
   public interface INumberConverter
   {
      /// <summary>
      /// Determines whether this instance can convert the specified input 
      /// from a binary to a decimal number.
      /// </summary>
      /// <param name="input">The input. If it equals a binary number, it can be converted.</param>
      /// <returns>
      ///   <c>true</c> if this instance can convert the specified input; otherwise, <c>false</c>.
      /// </returns>
      bool CanConvertBinaryToDecimal(string input);

      /// <summary>
      /// Converts the input to its decimal representation. 
      /// Make sure conversion is possible by calling the corresponding CanConvert method.
      /// </summary>
      /// <param name="input">The binary number to convert.</param>
      /// <returns>The input number in its decimal representation.</returns>
      string ConvertBinaryToDecimal(string input);
   }
}
