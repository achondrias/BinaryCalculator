﻿namespace Calculator.WinForms
{
   using System;
   using System.Linq;
   using System.Windows.Forms;
   using Base;
   using Base.Operations;
   using ValueConverter;
   using View;

   /// <summary>
   /// The main program. Does the set- and cleanup the application requires.
   /// </summary>
   public static class Program
   {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      public static void Main()
      {
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);

         var operations = new Operation[]
         {
            new Addition(),
            new Multiplication(),
            new Subtraction(),
            new Division()
         };
         ICalculator calculator = new Calculator(operations);
         INumberConverter converter = new BinaryDecimalConverter();
         var view = new MainWindow(operations.Select(op => op.Sign));
         view.BinaryOperator = Convert.ToString(operations.First().Sign);
         var presenter = new Presenter.Presenter(calculator, view, converter);

         Application.Run(view);
      }
   }
}
