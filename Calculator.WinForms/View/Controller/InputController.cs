﻿namespace Calculator.WinForms.View.Controller
{
   using System.Windows.Forms;

   /// <summary>
   /// Offers general input-related functionality.
   /// </summary>
   internal static class InputController
   {
      /// <summary>
      /// Defines which key corresponds to the reset command.
      /// </summary>
      private static readonly Keys ResetCommand = Keys.Escape;

      /// <summary>
      /// Determines whether the specified key character is backspace.
      /// </summary>
      /// <param name="input">The character to evaluate.</param>
      /// <returns><c>true</c> if the specified key character is backspace; otherwise, <c>false</c>.</returns>
      internal static bool IsBackspace(char input)
      {
         return input == '\b';
      }

      /// <summary>
      /// Determines whether the specified key data equals the set reset command.
      /// </summary>
      /// <param name="input">The key data.</param>
      /// <returns><c>true</c> if the specified key data equals the reset command; 
      /// otherwise, <c>false</c>.</returns>
      internal static bool IsResetCommand(Keys input)
      {
         return input == InputController.ResetCommand;
      }
   }
}
