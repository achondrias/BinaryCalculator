﻿namespace Calculator.WinForms.View.Controller
{
   using System.Windows.Forms;

   /// <summary>
   /// Offers general functionality for Controls.
   /// </summary>
   internal static class ControlController
   {
      /// <summary>
      /// Sets the text to zero.
      /// </summary>
      /// <param name="control">The text property of this control will be set to zero.</param>
      internal static void SetTextToZero(Control control)
      {
         control.Text = "0";
      }
   }
}
