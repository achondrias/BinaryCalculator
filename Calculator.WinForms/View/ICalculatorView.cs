﻿namespace Calculator.WinForms.View
{
   using System.ComponentModel;

   /// <summary>
   /// Defines a UI for a calculator that displays its values in binary and decimal form at the same time.
   /// </summary>
   /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
   public interface ICalculatorView : INotifyPropertyChanged
   {
      /// <summary>
      /// Gets or sets the binary operator.
      /// </summary>
      /// <value>The binary operator.</value>
      string BinaryOperator { get; set; }

      /// <summary>
      /// Gets or sets the binary left operand.
      /// </summary>
      /// <value>The binary left operand.</value>
      string BinaryLeftOperand { get; set; }

      /// <summary>
      /// Gets or sets the binary right operand.
      /// </summary>
      /// <value>The binary right operand.</value>
      string BinaryRightOperand { get; set; }

      /// <summary>
      /// Gets or sets the binary result.
      /// </summary>
      /// <value>The binary result.</value>
      string BinaryResult { get; set; }

      /// <summary>
      /// Gets or sets the decimal operator.
      /// </summary>
      /// <value>The decimal operator.</value>
      string DecimalOperator { get; set; }

      /// <summary>
      /// Gets or sets the decimal left operand.
      /// </summary>
      /// <value>The decimal left operand.</value>
      string DecimalLeftOperand { get; set; }

      /// <summary>
      /// Gets or sets the decimal right operand.
      /// </summary>
      /// <value>The decimal right operand.</value>
      string DecimalRightOperand { get; set; }

      /// <summary>
      /// Gets or sets the decimal result.
      /// </summary>
      /// <value>The decimal result.</value>
      string DecimalResult { get; set; }

      /// <summary>
      /// Shows the error message.
      /// </summary>
      /// <param name="errorMessage">The error message.</param>
      void ShowErrorMessage(string errorMessage);
   }
}