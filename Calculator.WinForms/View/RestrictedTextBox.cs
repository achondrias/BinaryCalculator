﻿namespace Calculator.WinForms.View
{
   using System.Linq;
   using System.Windows.Forms;
   using Controller;

   /// <summary>
   /// A TextBox that limits the symbols which can be entered.
   /// It does not prevent the Text property from being empty.
   /// </summary>
   /// <seealso cref="System.Windows.Forms.TextBox" />
   /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
   internal class RestrictedTextBox : TextBox
   {
      /// <summary>
      /// The allowed values. Any other characters cannot be added to the text field.
      /// </summary>
      private string allowedValues;

      /// <summary>
      /// Initializes a new instance of the <see cref="RestrictedTextBox"/> class.
      /// Registers the necessary event handler for filtering keyboard input.
      /// </summary>
      public RestrictedTextBox()
      {
         this.KeyPress += this.RestrictedTextBox_KeyPress;
      }

      /// <summary>
      /// Gets or sets the allowed values.
      /// </summary>
      /// <value>The allowed values.</value>
      public string AllowedValues
      {
         get
         {
            return this.allowedValues;
         }

         set
         {
            this.allowedValues = value == null ? string.Empty : value;
         }
      }

      /// <summary>
      /// Handles the KeyPress event of the RestrictedTextBox control.
      /// </summary>
      /// <param name="sender">The source of the event, ergo this instance.</param>
      /// <param name="e">The <see cref="KeyPressEventArgs"/> instance containing the keys data.</param>
      protected void RestrictedTextBox_KeyPress(object sender, KeyPressEventArgs e)
      {
         if (!this.allowedValues.Contains(e.KeyChar) && !InputController.IsBackspace(e.KeyChar))
         {
            e.Handled = true;
         }
      }
   }
}