﻿namespace Calculator.WinForms.View
{
   /// <summary>
   /// Auto generated part of this class.
   /// </summary>
   /// <seealso cref="System.Windows.Forms.Form" />
   /// <seealso cref="ICalculatorView" />
   public partial class MainWindow
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.backgroundBinCalculation = new System.Windows.Forms.Panel();
         this.binaryOperator = new RestrictedTextBox();
         this.binaryRightOperand = new RestrictedTextBox();
         this.binaryLeftOperand = new RestrictedTextBox();
         this.backgroundBinResult = new System.Windows.Forms.Panel();
         this.binaryResult = new RestrictedTextBox();
         this.backgroundDecCalculation = new System.Windows.Forms.Panel();
         this.decimalOperator = new RestrictedTextBox();
         this.decimalRightOperand = new RestrictedTextBox();
         this.decimalLeftOperand = new RestrictedTextBox();
         this.backgroundDecResult = new System.Windows.Forms.Panel();
         this.decimalResult = new RestrictedTextBox();
         this.backgroundBinCalculation.SuspendLayout();
         this.backgroundBinResult.SuspendLayout();
         this.backgroundDecCalculation.SuspendLayout();
         this.backgroundDecResult.SuspendLayout();
         this.SuspendLayout();
         // 
         // backgroundBinCalculation
         // 
         this.backgroundBinCalculation.BackColor = System.Drawing.Color.Black;
         this.backgroundBinCalculation.Controls.Add(this.binaryOperator);
         this.backgroundBinCalculation.Controls.Add(this.binaryRightOperand);
         this.backgroundBinCalculation.Controls.Add(this.binaryLeftOperand);
         this.backgroundBinCalculation.Location = new System.Drawing.Point(0, 0);
         this.backgroundBinCalculation.Name = "backgroundBinCalculation";
         this.backgroundBinCalculation.Size = new System.Drawing.Size(514, 206);
         this.backgroundBinCalculation.TabIndex = 13;
         // 
         // binaryOperator
         // 
         this.binaryOperator.AllowedValues = "";
         this.binaryOperator.BackColor = System.Drawing.Color.Black;
         this.binaryOperator.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.binaryOperator.Font = new System.Drawing.Font("OCR A Extended", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.binaryOperator.ForeColor = System.Drawing.Color.OrangeRed;
         this.binaryOperator.Location = new System.Drawing.Point(240, 37);
         this.binaryOperator.Margin = new System.Windows.Forms.Padding(0);
         this.binaryOperator.Multiline = true;
         this.binaryOperator.Name = "binaryOperator";
         this.binaryOperator.ReadOnly = true;
         this.binaryOperator.Size = new System.Drawing.Size(34, 68);
         this.binaryOperator.TabIndex = 4;
         this.binaryOperator.TabStop = false;
         // 
         // binaryRightOperand
         // 
         this.binaryRightOperand.AllowedValues = "01";
         this.binaryRightOperand.BackColor = System.Drawing.Color.Black;
         this.binaryRightOperand.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.binaryRightOperand.Font = new System.Drawing.Font("OCR A Extended", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.binaryRightOperand.ForeColor = System.Drawing.Color.OrangeRed;
         this.binaryRightOperand.Location = new System.Drawing.Point(308, 6);
         this.binaryRightOperand.Margin = new System.Windows.Forms.Padding(0);
         this.binaryRightOperand.Multiline = true;
         this.binaryRightOperand.Name = "binaryRightOperand";
         this.binaryRightOperand.Size = new System.Drawing.Size(197, 197);
         this.binaryRightOperand.TabIndex = 1;
         this.binaryRightOperand.Text = "0";
         // 
         // binaryLeftOperand
         // 
         this.binaryLeftOperand.AllowedValues = "01";
         this.binaryLeftOperand.BackColor = System.Drawing.Color.Black;
         this.binaryLeftOperand.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.binaryLeftOperand.Font = new System.Drawing.Font("OCR A Extended", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.binaryLeftOperand.ForeColor = System.Drawing.Color.OrangeRed;
         this.binaryLeftOperand.Location = new System.Drawing.Point(9, 6);
         this.binaryLeftOperand.Margin = new System.Windows.Forms.Padding(0);
         this.binaryLeftOperand.Multiline = true;
         this.binaryLeftOperand.Name = "binaryLeftOperand";
         this.binaryLeftOperand.Size = new System.Drawing.Size(197, 197);
         this.binaryLeftOperand.TabIndex = 0;
         this.binaryLeftOperand.Text = "0";
         // 
         // backgroundBinResult
         // 
         this.backgroundBinResult.BackColor = System.Drawing.Color.Black;
         this.backgroundBinResult.Controls.Add(this.binaryResult);
         this.backgroundBinResult.Location = new System.Drawing.Point(0, 207);
         this.backgroundBinResult.Name = "backgroundBinResult";
         this.backgroundBinResult.Size = new System.Drawing.Size(514, 161);
         this.backgroundBinResult.TabIndex = 14;
         // 
         // binaryResult
         // 
         this.binaryResult.AllowedValues = "01";
         this.binaryResult.BackColor = System.Drawing.Color.Black;
         this.binaryResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.binaryResult.Font = new System.Drawing.Font("OCR A Extended", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.binaryResult.ForeColor = System.Drawing.Color.OrangeRed;
         this.binaryResult.Location = new System.Drawing.Point(9, 10);
         this.binaryResult.Margin = new System.Windows.Forms.Padding(0);
         this.binaryResult.Multiline = true;
         this.binaryResult.Name = "binaryResult";
         this.binaryResult.ReadOnly = true;
         this.binaryResult.Size = new System.Drawing.Size(496, 138);
         this.binaryResult.TabIndex = 4;
         this.binaryResult.TabStop = false;
         this.binaryResult.Text = "0";
         // 
         // backgroundDecCalculation
         // 
         this.backgroundDecCalculation.BackColor = System.Drawing.Color.Black;
         this.backgroundDecCalculation.Controls.Add(this.decimalOperator);
         this.backgroundDecCalculation.Controls.Add(this.decimalRightOperand);
         this.backgroundDecCalculation.Controls.Add(this.decimalLeftOperand);
         this.backgroundDecCalculation.Location = new System.Drawing.Point(0, 371);
         this.backgroundDecCalculation.Name = "backgroundDecCalculation";
         this.backgroundDecCalculation.Size = new System.Drawing.Size(514, 111);
         this.backgroundDecCalculation.TabIndex = 16;
         // 
         // decimalOperator
         // 
         this.decimalOperator.AllowedValues = "";
         this.decimalOperator.BackColor = System.Drawing.Color.Black;
         this.decimalOperator.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.decimalOperator.Font = new System.Drawing.Font("OCR A Extended", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.decimalOperator.ForeColor = System.Drawing.Color.OrangeRed;
         this.decimalOperator.Location = new System.Drawing.Point(240, 10);
         this.decimalOperator.Margin = new System.Windows.Forms.Padding(0);
         this.decimalOperator.Multiline = true;
         this.decimalOperator.Name = "decimalOperator";
         this.decimalOperator.ReadOnly = true;
         this.decimalOperator.Size = new System.Drawing.Size(34, 68);
         this.decimalOperator.TabIndex = 5;
         this.decimalOperator.TabStop = false;
         // 
         // decimalRightOperand
         // 
         this.decimalRightOperand.AllowedValues = "0123456789";
         this.decimalRightOperand.BackColor = System.Drawing.Color.Black;
         this.decimalRightOperand.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.decimalRightOperand.Font = new System.Drawing.Font("OCR A Extended", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.decimalRightOperand.ForeColor = System.Drawing.Color.OrangeRed;
         this.decimalRightOperand.Location = new System.Drawing.Point(308, 10);
         this.decimalRightOperand.Margin = new System.Windows.Forms.Padding(0);
         this.decimalRightOperand.Multiline = true;
         this.decimalRightOperand.Name = "decimalRightOperand";
         this.decimalRightOperand.ReadOnly = true;
         this.decimalRightOperand.Size = new System.Drawing.Size(197, 90);
         this.decimalRightOperand.TabIndex = 3;
         this.decimalRightOperand.TabStop = false;
         this.decimalRightOperand.Text = "0";
         // 
         // decimalLeftOperand
         // 
         this.decimalLeftOperand.AllowedValues = "0123456789";
         this.decimalLeftOperand.BackColor = System.Drawing.Color.Black;
         this.decimalLeftOperand.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.decimalLeftOperand.Font = new System.Drawing.Font("OCR A Extended", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.decimalLeftOperand.ForeColor = System.Drawing.Color.OrangeRed;
         this.decimalLeftOperand.Location = new System.Drawing.Point(9, 10);
         this.decimalLeftOperand.Margin = new System.Windows.Forms.Padding(0);
         this.decimalLeftOperand.Multiline = true;
         this.decimalLeftOperand.Name = "decimalLeftOperand";
         this.decimalLeftOperand.ReadOnly = true;
         this.decimalLeftOperand.Size = new System.Drawing.Size(197, 90);
         this.decimalLeftOperand.TabIndex = 2;
         this.decimalLeftOperand.TabStop = false;
         this.decimalLeftOperand.Text = "0";
         // 
         // backgroundDecResult
         // 
         this.backgroundDecResult.BackColor = System.Drawing.Color.Black;
         this.backgroundDecResult.Controls.Add(this.decimalResult);
         this.backgroundDecResult.ForeColor = System.Drawing.Color.OrangeRed;
         this.backgroundDecResult.Location = new System.Drawing.Point(0, 483);
         this.backgroundDecResult.Name = "backgroundDecResult";
         this.backgroundDecResult.Size = new System.Drawing.Size(514, 78);
         this.backgroundDecResult.TabIndex = 18;
         // 
         // decimalResult
         // 
         this.decimalResult.AllowedValues = "0123456789";
         this.decimalResult.BackColor = System.Drawing.Color.Black;
         this.decimalResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
         this.decimalResult.Font = new System.Drawing.Font("OCR A Extended", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.decimalResult.ForeColor = System.Drawing.Color.OrangeRed;
         this.decimalResult.Location = new System.Drawing.Point(9, 7);
         this.decimalResult.Margin = new System.Windows.Forms.Padding(0);
         this.decimalResult.Multiline = true;
         this.decimalResult.Name = "decimalResult";
         this.decimalResult.ReadOnly = true;
         this.decimalResult.Size = new System.Drawing.Size(496, 68);
         this.decimalResult.TabIndex = 6;
         this.decimalResult.TabStop = false;
         this.decimalResult.Text = "0";
         // 
         // MainWindow
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.DimGray;
         this.ClientSize = new System.Drawing.Size(514, 562);
         this.Controls.Add(this.backgroundBinCalculation);
         this.Controls.Add(this.backgroundBinResult);
         this.Controls.Add(this.backgroundDecResult);
         this.Controls.Add(this.backgroundDecCalculation);
         this.MaximumSize = new System.Drawing.Size(530, 600);
         this.MinimumSize = new System.Drawing.Size(530, 600);
         this.Name = "MainWindow";
         this.Text = "Binary Calculator";
         this.backgroundBinCalculation.ResumeLayout(false);
         this.backgroundBinCalculation.PerformLayout();
         this.backgroundBinResult.ResumeLayout(false);
         this.backgroundBinResult.PerformLayout();
         this.backgroundDecCalculation.ResumeLayout(false);
         this.backgroundDecCalculation.PerformLayout();
         this.backgroundDecResult.ResumeLayout(false);
         this.backgroundDecResult.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion
      private System.Windows.Forms.Panel backgroundBinCalculation;
      private System.Windows.Forms.Panel backgroundBinResult;
      private System.Windows.Forms.Panel backgroundDecCalculation;
      private System.Windows.Forms.Panel backgroundDecResult;
      private RestrictedTextBox decimalLeftOperand;
      private RestrictedTextBox binaryRightOperand;
      private RestrictedTextBox binaryLeftOperand;
      private RestrictedTextBox binaryOperator;
      private RestrictedTextBox binaryResult;
      private RestrictedTextBox decimalOperator;
      private RestrictedTextBox decimalRightOperand;
      private RestrictedTextBox decimalResult;
   }
}