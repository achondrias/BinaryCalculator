﻿namespace Calculator.WinForms.View
{
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.Linq;
   using System.Runtime.CompilerServices;
   using System.Windows.Forms;
   using Controller;

   /// <summary>
   /// An implementation of ICalculatorView.
   /// Does not raise property changed events for any 'decimal' controls nor result controls.
   /// </summary>
   /// <seealso cref="System.Windows.Forms.Form" />
   /// <seealso cref="UI.ICalculatorView" />
   public sealed partial class MainWindow : Form, ICalculatorView
   {
      #region Fields
      /// <summary>
      /// The available operator signs. 
      /// Must correspond to the operations available in the calculator.
      /// </summary>
      private readonly IEnumerable<char> availableOperatorSigns;
      #endregion

      #region Constructors
      /// <summary>
      /// Initializes a new instance of the <see cref="MainWindow"/> class.
      /// </summary>
      /// <param name="operatorSigns">The operator signs.</param>
      /// <exception cref="ArgumentNullException">
      /// <paramref name="operatorSigns"/> must not be empty!</exception>
      public MainWindow(IEnumerable<char> operatorSigns)
      {
         if (operatorSigns == null)
         {
            throw new ArgumentNullException(nameof(operatorSigns) + " must not be null.");
         }

         this.InitializeComponent();
         this.SetEventHandlers();
         this.availableOperatorSigns = operatorSigns;
      }
      #endregion

      #region Events
      /// <summary>
      /// Occurs when a property changed. Used for communicating changes outside.
      /// </summary>
      public event PropertyChangedEventHandler PropertyChanged;
      #endregion

      #region Properties
      /// <summary>
      /// Gets or sets the binary left operand.
      /// </summary>
      /// <value>The binary left operand.</value>
      public string BinaryLeftOperand
      {
         get
         {
            return this.binaryLeftOperand.Text;
         }

         set
         {
            if (value != this.binaryLeftOperand.Text)
            {
               this.binaryLeftOperand.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the binary operator.
      /// </summary>
      /// <value>The binary operator.</value>
      public string BinaryOperator
      {
         get
         {
            return this.binaryOperator.Text;
         }

         set
         {
            if (value != this.binaryOperator.Text)
            {
               this.binaryOperator.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the binary result.
      /// </summary>
      /// <value>The binary result.</value>
      public string BinaryResult
      {
         get
         {
            return this.binaryResult.Text;
         }

         set
         {
            if (value != this.binaryResult.Text)
            {
               this.binaryResult.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the binary right operand.
      /// </summary>
      /// <value>The binary right operand.</value>
      public string BinaryRightOperand
      {
         get
         {
            return this.binaryRightOperand.Text;
         }

         set
         {
            if (value != this.binaryRightOperand.Text)
            {
               this.binaryRightOperand.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the decimal left operand.
      /// </summary>
      /// <value>The decimal left operand.</value>
      public string DecimalLeftOperand
      {
         get
         {
            return this.decimalLeftOperand.Text;
         }

         set
         {
            if (value != this.decimalLeftOperand.Text)
            {
               this.decimalLeftOperand.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the decimal operator.
      /// </summary>
      /// <value>The decimal operator.</value>
      public string DecimalOperator
      {
         get
         {
            return this.decimalOperator.Text;
         }

         set
         {
            if (value != this.decimalOperator.Text)
            {
               this.decimalOperator.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the decimal result.
      /// </summary>
      /// <value>The decimal result.</value>
      public string DecimalResult
      {
         get
         {
            return this.decimalResult.Text;
         }

         set
         {
            if (value != this.decimalResult.Text)
            {
               this.decimalResult.Text = value;
            }
         }
      }

      /// <summary>
      /// Gets or sets the decimal right operand.
      /// </summary>
      /// <value>The decimal right operand.</value>
      public string DecimalRightOperand
      {
         get
         {
            return this.decimalRightOperand.Text;
         }

         set
         {
            if (value != this.decimalRightOperand.Text)
            {
               this.decimalRightOperand.Text = value;
            }
         }
      }
      #endregion

      #region Methods      
      /// <summary>
      /// Shows the error message.
      /// </summary>
      /// <param name="errorMessage">The error message.</param>
      public void ShowErrorMessage(string errorMessage)
      {
         MessageBox.Show(errorMessage);
      }

      /// <summary>
      /// Handles the <see cref="E:PropertyChanged" /> event.
      /// </summary>
      /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
      private void OnPropertyChanged(PropertyChangedEventArgs e)
      {
         this.PropertyChanged?.Invoke(this, e);
      }

      /// <summary>
      /// Sets the property. Does an equality check and raises the PropertyChanged event correctly.
      /// </summary>
      /// <typeparam name="T">No need to set this explicitly.</typeparam>
      /// <param name="field">The field this property is connected to.</param>
      /// <param name="newValue">The new value.</param>
      /// <param name="propertyName">Name of the property. Inferred automatically.</param>
      /// <returns><c>true</c> if a new value was, <c>false</c> otherwise.</returns>
      private bool SetProperty<T>(
         ref T field, 
         T newValue, 
         [CallerMemberName] string propertyName = null)
      {
         if (!EqualityComparer<T>.Default.Equals(field, newValue))
         {
            field = newValue;
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
         }

         return false;
      }

      /// <summary>
      /// Sets all event handler.
      /// </summary>
      private void SetEventHandlers()
      {
         this.binaryLeftOperand.KeyDown += this.BinaryOperand_KeyDown;
         this.binaryRightOperand.KeyDown += this.BinaryOperand_KeyDown;
         this.binaryLeftOperand.KeyPress += this.BinaryOperand_KeyPress;
         this.binaryRightOperand.KeyPress += this.BinaryOperand_KeyPress;
         this.binaryLeftOperand.TextChanged += this.BinaryLeftOperand_TextChanged;
         this.binaryRightOperand.TextChanged += this.BinaryRightOperand_TextChanged;
         this.binaryOperator.TextChanged += this.BinaryOperator_TextChanged;
      }

      /// <summary>
      /// Handles the TextChanged event of the BinaryOperator control.
      /// Calls OnPropertyChanged if its text is neither null nor empty.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
      private void BinaryOperator_TextChanged(object sender, EventArgs e)
      {
         if (!string.IsNullOrEmpty(this.binaryOperator.Text))
         {
            this.OnPropertyChanged(new PropertyChangedEventArgs(
               nameof(this.BinaryOperator)));
         }
      }

      /// <summary>
      /// Handles the TextChanged event of the BinaryRightOperand control.
      /// Calls OnPropertyChanged if its text is neither null nor empty.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
      private void BinaryRightOperand_TextChanged(object sender, EventArgs e)
      {
         if (!string.IsNullOrEmpty(this.binaryRightOperand.Text))
         {
            this.OnPropertyChanged(new PropertyChangedEventArgs(
            nameof(this.BinaryRightOperand)));
         }
      }

      /// <summary>
      /// Handles the TextChanged event of the BinaryLeftOperand control.
      /// Calls OnPropertyChanged if its text is neither null nor empty.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
      private void BinaryLeftOperand_TextChanged(object sender, EventArgs e)
      {
         if (!string.IsNullOrEmpty(this.binaryLeftOperand.Text))
         {
            this.OnPropertyChanged(new PropertyChangedEventArgs(
            nameof(this.BinaryLeftOperand)));
         }
      }

      /// <summary>
      /// Handles the KeyDown event of the BinaryOperand control.
      /// Takes action if a reset command was sent.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
      private void BinaryOperand_KeyDown(object sender, KeyEventArgs e)
      {
         if (InputController.IsResetCommand(e.KeyData))
         {
            this.Reset();
         }
      }

      /// <summary>
      /// Handles the KeyPress event of the BinaryOperand control.
      /// Takes action if an operator sign was entered.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="KeyPressEventArgs"/> instance containing the event data.</param>
      private void BinaryOperand_KeyPress(object sender, KeyPressEventArgs e)
      {
         if (this.availableOperatorSigns.Contains(e.KeyChar))
         {
            this.SetOperators(e.KeyChar);
         }
      }

      /// <summary>
      /// Sets the operators.
      /// </summary>
      /// <param name="operatorSign">The operator sign.</param>
      private void SetOperators(char operatorSign)
      {
         this.decimalOperator.Text = Convert.ToString(operatorSign);
         this.binaryOperator.Text = Convert.ToString(operatorSign);
         this.binaryRightOperand.Focus();
      }

      /// <summary>
      /// Resets this instance to its initial state.
      /// </summary>
      private void Reset()
      {
         this.SetOperators(this.availableOperatorSigns.First());
         ControlController.SetTextToZero(this.binaryLeftOperand);
         ControlController.SetTextToZero(this.binaryRightOperand);
         this.binaryLeftOperand.Focus();
      }
      #endregion
   }
}