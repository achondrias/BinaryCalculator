﻿namespace Calculator.WinForms.Presenter
{
   using System;
   using System.ComponentModel;
   using System.Linq;
   using Base;
   using ValueConverter;
   using View;

   /// <summary>
   /// Synchronizes the model and the UI.
   /// </summary>
   internal class Presenter
   {
      /// <summary>
      /// The calculator containing the algorithms.
      /// </summary>
      private ICalculator calculator;

      /// <summary>
      /// The view used for user interaction.
      /// </summary>
      private ICalculatorView view;

      /// <summary>
      /// The converter used for converting between number types.
      /// </summary>
      private INumberConverter converter;

      /// <summary>
      /// Initializes a new instance of the <see cref="Presenter"/> class.
      /// </summary>
      /// <param name="calculator">The calculator.</param>
      /// <param name="view">The view.</param>
      /// <param name="converter">The converter.</param>
      /// <exception cref="ArgumentNullException">
      /// Thrown when on of the arguments is null.
      /// </exception>
      public Presenter(ICalculator calculator, ICalculatorView view, INumberConverter converter)
      {
         if (calculator == null)
         {
            throw new ArgumentNullException(nameof(calculator) + " must not be null.");
         }

         if (view == null)
         {
            throw new ArgumentNullException(nameof(view) + " must not be null.");
         }

         if (converter == null)
         {
            throw new ArgumentNullException(nameof(converter) + " must not be null.");
         }

         this.calculator = calculator;
         this.converter = converter;
         this.view = view;

         this.view.PropertyChanged += this.View_PropertyChanged;
      }

      /// <summary>
      /// Updates the decimal number specified by the property name.
      /// </summary>
      /// <param name="propertyName">Name of the property.</param>
      /// <param name="newValue">The new value.</param>
      /// <exception cref="ArgumentException">
      /// The specified property is unknown: " + propertyName
      /// </exception>
      private void UpdateDecimalNumber(string propertyName, string newValue)
      {
         switch (propertyName)
         {
            case nameof(ICalculatorView.DecimalLeftOperand):
               this.view.DecimalLeftOperand = newValue;
               return;

            case nameof(ICalculatorView.DecimalRightOperand):
               this.view.DecimalRightOperand = newValue;
               return;

            case nameof(ICalculatorView.DecimalResult):
               this.view.DecimalResult = newValue;
               return;

            default:
               throw new ArgumentException(
                  "The specified property is unknown: " + propertyName,
                  nameof(propertyName));
         }
      }

      /// <summary>
      /// Handles the PropertyChanged event of the View control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">
      /// The <see cref="PropertyChangedEventArgs"/> instance containing the event data.
      /// </param>
      private void View_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         bool canCalculate;
         string leftOperand = this.view.BinaryLeftOperand;
         string rightOperand = this.view.BinaryRightOperand;
         char operatorSign = this.view.BinaryOperator.First();
         string calculationResult = null;
         string errorMessage = null;

         switch (e.PropertyName)
         {
            case nameof(this.view.BinaryLeftOperand):
               this.view.DecimalLeftOperand = this.converter.ConvertBinaryToDecimal(leftOperand);
               break;

            case nameof(this.view.BinaryRightOperand):
               this.view.DecimalRightOperand = this.converter.ConvertBinaryToDecimal(rightOperand);
               break;

            default:
               break;
         }

         canCalculate = this.calculator.CanCalculate(
            operatorSign,
            leftOperand,
            rightOperand,
            out errorMessage);

         if (canCalculate)
         {
            calculationResult = this.calculator.Calculate(operatorSign, leftOperand, rightOperand);
            this.view.BinaryResult = calculationResult.ToString();
            this.view.DecimalResult = this.converter.ConvertBinaryToDecimal(calculationResult);
         }
         else
         {
            this.view.ShowErrorMessage(errorMessage);
         }
      }
   }
}