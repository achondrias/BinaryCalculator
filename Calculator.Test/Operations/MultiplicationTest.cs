﻿namespace Calculator.Test.Operations
{
   using System;
   using System.Collections.Generic;
   using Microsoft.VisualStudio.TestTools.UnitTesting;
   using Base;
   using Base.Operations;

   /// <summary>
   /// Unit tests for Base.Operations.Multiplication
   /// </summary>
   [TestClass]
   public class MultiplicationTest
   {
      private char operatorSign = '*';

      private ICalculator Calculator
      {
         get
         {
            var operations = new List<Operation>() { new Multiplication() };

            return new Calculator(operations);
         }
      }

      [TestMethod]
      public void ZeroZeroEqualsZero()
      {
         var leftOperand = "0";
         var rightOperand = "0";
         var expected = "0";

         var result = this.Calculator.Calculate(this.operatorSign, leftOperand, rightOperand);

         Assert.AreEqual(expected, result);
      }

      [TestMethod]
      public void OneZeroEqualsZero()
      {
         var leftOperand = "1";
         var rightOperand = "0";
         var expected = "0";

         var result = this.Calculator.Calculate(this.operatorSign, leftOperand, rightOperand);
         var resultReverseOrder = this.Calculator.Calculate(
            this.operatorSign,
            rightOperand,
            leftOperand);

         Assert.AreEqual(expected, result);
         Assert.AreEqual(expected, resultReverseOrder);
      }

      [TestMethod]
      public void OneOneEqualsOne()
      {
         var leftOperand = "1";
         var rightOperand = "1";
         var expected = "1";

         var result = this.Calculator.Calculate(this.operatorSign, leftOperand, rightOperand);

         Assert.AreEqual(expected, result);
      }

      [TestMethod]
      public void ArbitraryNumbers()
      {
         int leftOperand, rightOperand, result, startValue = 12345;
         string binaryLeftOperand, binaryRightOperand, binaryResult, expected;

         for (int i = 0; i < 8; i++)
         {
            leftOperand = startValue + i;
            binaryLeftOperand = Convert.ToString(leftOperand, 2);

            for (int k = 0; k < 8; k++)
            {
               rightOperand = startValue + k;
               binaryRightOperand = Convert.ToString(rightOperand, 2);
               result = leftOperand * rightOperand;
               expected = Convert.ToString(result, 2);
               binaryResult = this.Calculator.Calculate(
                  this.operatorSign,
                  binaryLeftOperand,
                  binaryRightOperand);

               Assert.AreEqual(expected, binaryResult);
            }
         }
      }
   }
}