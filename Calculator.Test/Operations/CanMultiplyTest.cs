﻿namespace Calculator.Test.Operations
{
   using System.Collections.Generic;
   using Base;
   using Base.Operations;
   using Microsoft.VisualStudio.TestTools.UnitTesting;

   [TestClass]
   public class CanMultiplyTest
   {
      private char operatorSign = '*';

      private ICalculator calculator
      {
         get
         {
            var operations = new List<Operation>()
            {
               new Multiplication()
            };

            return new Calculator(operations);
         }
      }

      [TestMethod]
      public void ZeroZeroCanCalculate()
      {
         var leftOperand = "0";
         var rightOperand = "0";
         string message;

         var result = this.calculator.CanCalculate(
            this.operatorSign,
            leftOperand,
            rightOperand,
            out message);

         Assert.IsTrue(result);
         Assert.IsNull(message);
      }

      [TestMethod]
      public void OneZeroCanCalculate()
      {
         var leftOperand = "1";
         var rightOperand = "0";
         string message;

         var result = this.calculator.CanCalculate(this.operatorSign, leftOperand, rightOperand, out message);

         Assert.IsTrue(result);
         Assert.IsNull(message);
      }

      [TestMethod]
      public void OneOneCanCalculate()
      {
         var leftOperand = "1";
         var rightOperand = "1";
         string message;

         var result = this.calculator.CanCalculate(this.operatorSign, leftOperand, rightOperand, out message);

         Assert.IsTrue(result);
         Assert.IsNull(message);
      }

      [TestMethod]
      public void ArbitraryNumbersCanCalculate()
      {
         bool result;
         string message;
         var operands = new List<string>()
         {
            "10000001000", // 1032
            "10000001001",
            "10000001010",
            "10000001011",
            "10000001100",
            "10000001101",
            "10000001110",
            "10000001111" // 1039
         };

         for (int i = 0; i < 8; i++)
         {
            for (int k = 0; k < 8; k++)
            {
               result = this.calculator.CanCalculate(this.operatorSign, operands[i], operands[k], out message);

               Assert.IsTrue(result);
               Assert.IsNull(message);
            }
         }
      }

      [TestMethod]
      public void NullOperandsCannotCalculate()
      {
         var validOperand = "1";
         string message = null;

         var result = this.calculator.CanCalculate(
            this.operatorSign,
            null,
            validOperand,
            out message);

         Assert.IsFalse(result);
         Assert.AreEqual("Left operand invalid. Null cannot be converted.", message);

         result = this.calculator.CanCalculate(
            this.operatorSign,
            validOperand,
            null,
            out message);

         Assert.IsFalse(result);
         Assert.AreEqual("Right operand invalid. Null cannot be converted.", message);

         result = this.calculator.CanCalculate(
            this.operatorSign,
            null,
            null,
            out message);

         Assert.IsFalse(result);
         Assert.AreEqual(
            "Both operands are invalid." +
            " Left: Null cannot be converted. " +
            "Right: Null cannot be converted.",
            message);
      }
   }
}