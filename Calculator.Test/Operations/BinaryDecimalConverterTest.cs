﻿namespace Calculator.Test.Operations
{
   using System;
   using System.Collections.Generic;
   using Microsoft.VisualStudio.TestTools.UnitTesting;
   using ValueConverter;

   [TestClass]
   public class BinaryDecimalConverterTest
   {
      private INumberConverter Converter
      {
         get
         {
            return new BinaryDecimalConverter();
         }
      }

      [TestMethod]
      public void ZeroToNine()
      {
         string converted;
         int i = 0;
         var binaryNumbers = new List<string>()
         {
            "0",
            "1",
            "10",
            "11",
            "100",
            "101",
            "110",
            "111",
            "1000",
            "1001"
         };

         foreach (string number in binaryNumbers)
         {
            converted = this.Converter.ConvertBinaryToDecimal(number);
            Assert.AreEqual(converted, i.ToString());
            ++i;
         }
      }

      [TestMethod]
      public void ArbitraryNumbers()
      {
         string binary;

         for (int i = 123456789; i < 123456819; i++)
         {
            binary = Convert.ToString(i, 2);
            Assert.AreEqual(i.ToString(), this.Converter.ConvertBinaryToDecimal(binary));
         }
      }
   }
}
