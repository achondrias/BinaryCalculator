﻿namespace Calculator.Test
{
   using Microsoft.VisualStudio.TestTools.UnitTesting;

   /// <summary>
   /// Tests to make sure the framework is working.
   /// </summary>
   [TestClass]
   public class BaseTest
   {
      [TestMethod]
      public void FrameworkIsWorking()
      {
         Assert.AreEqual(1, 1);
         Assert.AreNotEqual(0, 1);
      }
   }
}