﻿namespace Calculator.Test.Calculator
{
   using System;
   using System.Collections.Generic;
   using Microsoft.VisualStudio.TestTools.UnitTesting;
   using Base;
   using Base.Operations;

   [TestClass]
   public class CalculatorTest
   {
      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void NullAsConstructorParamterThrows()
      {
         new Calculator(null);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentException))]
      public void EmptyListAsConstructorParamterThrows()
      {
         var emptyList = new List<Operation>() { };
         new Calculator(emptyList);
      }
   }
}